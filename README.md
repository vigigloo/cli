# Vigigloo CLI

## Installation

Install latest release version:

```shell
$ cargo install --git https://gitlab.com/vigigloo/cli.git --tag v1.4.0 -F cli
```

Install latest develop version:

```shell
$ cargo install --git https://gitlab.com/vigigloo/cli.git --branch develop -F cli
```

## Usage

### Config file

Default config file is "vigigloo.yml" in current path.\
The default config file can be overwritten with`VIGIGLOO_CONFIG_FILE`environment variable. It can be the path to the config file, or the path to a directory that contains a config file named "vigigloo.yml".\
All can be overwritten calling CLI with config :  `vigigloo [-c/--config <CONFIG_FILE>] <COMMAND>`.

### [Run](./src/run/README.md)

### [Check](./src/check/README.md)

### [Outdated](./src/sources/outdated/README.md)

### [Update](./src/sources/update/README.md)


### Inline Hints (Common to `update` & `outdated`)

In terraform files, inner dependencies in terraform modules can also be tracked using `Inline hints`. \
A `Inline Hint` is a templated comment : \
`# @vigigloo source "<FORMATED_URL>" "<VERSION_CONSTRAINT>"`
where ``FORMATED_URL`` can either be :
- a helm repository : ``helm+<REPOSITORY_URL>#name=<NAME>``
- a gitlab repository : ``terraform://<REPOSITORY_URL>``

``VERSION_CONSTRAINT`` must respect semver (if helm) ou terraform versioning format (if terraform).

Outdated and Update commands will consider the Inline Hint line as instruction to process the first next line.

<i>Example</i>
```sh
  module "mattermost" {
    source  = "gitlab.com/vigigloo/tools-k8s/mattermost"
    version = "0.1.0"

    chart_name    = "nginx-ingress"
    # @vigigloo source "helm+https://helm.nginx.com/stable#name=nginx-ingress" "> 0.10.0 , < 0.18.0"
    chart_version = "0.16.0"

    image_repository = "mattermost/mattermost-team-edition"
    # @vigigloo source "terraform://gitlab.com/vigigloo/tf-modules/k8slimitednamespace" "2.0.0"
    image_tag        = "1.0.0"

}
```
