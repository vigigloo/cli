// Usage: cargo run --example outdated-display
fn main() {
    use vigigloo::ops::outdated::display;
    use vigigloo::sources::{data, parse::Location, resolve::ResolvedSource, VersionedSource};

    let mock: Vec<_> = vec![
        Ok(ResolvedSource::new(
            VersionedSource::new(data::TerraformSource::new_provider("gitlabhq/gitlab"))
                .with_current("3.12.0")
                .with_required("3.12.0")
                .with_location(Location::new("versions.tf", 5, 0)),
            "4.2.0",
        )
        .with_wanted("3.15.0")),
        Ok(ResolvedSource::new(
            VersionedSource::new(data::TerraformSource::new_module(
                "gitlab.com/vigigloo/tf-modules/k8slimitednamespace",
            ))
            .with_location(Location::new("namespace.tf", 42, 0)),
            "0.3.6",
        )),
        Ok(ResolvedSource::new(
            VersionedSource::new(data::TerraformSource::new_module("longestaddresseveromgsuperlonglongestaddresseveromgsuperlonglongestaddresseveromgsuperlonglongestaddresseveromgsuperlonglongestaddresseveromgsuperlonglongestaddresseveromgsuperlong"))
            .with_location(Location::new("truc.tf", 42, 0))
            .with_required("0.1.0")
            .with_current("0.1.0"),
            "1.3.6"
        ).with_wanted("0.1.2")),
    ];

    display::display_results(mock.into());
}
