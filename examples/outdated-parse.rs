#[tokio::main]
async fn main() -> miette::Result<()> {
    use vigigloo::sources::parse;

    let (sources, _errors): (Vec<_>, Vec<_>) = parse::parse_cwd(&Default::default()).await.split();
    for source in sources {
        println!(
            "[{loc:?}] {address}: {version:?}",
            loc = source.source.loc.map(|loc| loc.to_string()),
            address = source.source.data.describe(),
            version = source.current,
        );
    }

    Ok(())
}
