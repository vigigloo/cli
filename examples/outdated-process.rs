// Usage: cargo run --features="cli" --example outdated-process
use miette::Result;

macro_rules! tag {
    ($tag:expr) => {
        vigigloo::sources::list::Tag::new_without_date($tag)
    };
}

macro_rules! tags {
    () => {
        Vec::new()
    };
    ($($tag:expr),+$(,)?) => {
        vec![$(tag!($tag)),+]
    };
}

fn main() -> Result<()> {
    use itertools::Itertools;

    use vigigloo::ops::outdated::display;
    use vigigloo::sources::{data, list::Tag, parse::Location, resolve, VersionedSource};

    struct Item {
        source: VersionedSource,
        tags: Vec<Tag>,
    }

    let mock: Vec<Item> = vec![
        Item {
            source: VersionedSource::new(data::TerraformSource::new_provider("gitlabhq/gitlab"))
                .with_required("> 0.2.55, <= 1.3.8")
                .with_current("> 0.2.55, <= 1.3.8")
                .with_location(Location::new("versions.tf", 5, 0)),
            tags: tags!["1.0.0", "1.2.0", "1.2.55", "1.2.8", "1.3.0", "1.3.9", "2.0.0"],
        },
        Item {
            source: VersionedSource::new(data::HelmSource::new("repository", "nginx"))
                .with_required("~1.2")
                .with_current("~1.2")
                .with_location(Location::new("Chart.yaml", 6, 14)),
            tags: tags!["1.0.0", "1.2.0", "1.2.1", "1.2.2", "1.2.3", "1.4.0", "1.14.2"],
        },
        Item {
            source: VersionedSource::new(data::TerraformSource::new_provider("gitlabhq/gitlab"))
                .with_location(Location::new("versions.tf", 5, 0))
                .with_required("> a.2.55 , <= 1.3.8")
                .with_current("> a.2.55 , <= 1.3.8"),
            tags: tags!["1.0.0", "1.2.0", "1.2.55", "1.2.8", "1.3.0", "1.3.9", "2.0.0"],
        },
        Item {
            source: VersionedSource::new(data::TerraformSource::new_provider("gitlabhq/gitlab"))
                .with_location(Location::new("versions.tf", 5, 0))
                .with_required("~> 2.0.0")
                .with_current("~> 2.0.0"),
            tags: tags!["1.0.0", "1.2.0", "1.2.55", "1.2.8", "1.3.0", "1.3.9", "2.0.0"],
        },
        Item {
            source: VersionedSource::new(data::TerraformSource::new_module(
                "gitlab.com/vigigloo/tf-modules/k8slimitednamespace",
            ))
            .with_location(Location::new("namespace.tf", 42, 9))
            .with_required("~> 1.2.1")
            .with_current("~> 1.2.1"),
            tags: tags!["1.0.0", "1.2.0", "1.2.55", "1.2.8", "1.3.0", "1.3.9", "2.0.0"],
        },
        Item {
            source: VersionedSource::new(data::TerraformSource::new_module(
                "gitlab.com/vigigloo/tf-modules/k8slimitednamespace",
            ))
            .with_location(Location::new("namespaceuh.tf", 42, 9)),
            tags: tags!["1.3.0", "2.0.0"],
        },
        Item {
            source: VersionedSource::new(data::TerraformSource::new_module(
                "gitlab.com/vigigloo/tf-modules/k8slimitednamespace",
            ))
            .with_location(Location::new("namespace.tf", 42, 9))
            .with_required("= 0.2.4-beta")
            .with_current("= 0.2.4-beta"),
            tags: tags!["0.1.0", "0.2.0", "0.2.4-beta"],
        },
        Item {
            source: VersionedSource::new(data::TerraformSource::new_module(
                "gitlab.com/vigigloo/tf-modules/k8slimitednamespace",
            ))
            .with_location(Location::new("namespace.tf", 42, 9))
            .with_required("= 0.2.4")
            .with_current("= 0.2.4"),
            tags: tags![],
        },
    ];

    // let mock_checked = process::process_result(mock);
    let sources: Vec<_> = mock
        .into_iter()
        .map(|item| resolve::resolve_tags(&item.source, &item.tags, &Default::default()))
        .filter_map_ok(std::convert::identity)
        .collect();
    display::display_results(sources.into());
    Ok(())
}
