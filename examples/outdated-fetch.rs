// Usage: cargo run --features "cli" --example outdated-fetch

#[tokio::main]
async fn main() -> miette::Result<()> {
    use vigigloo::sources::{data, list, VersionedSource};

    let sources = vec![
        VersionedSource::new(data::TerraformSource::new_provider("gitlabhq/gitlab")),
        VersionedSource::new(data::HelmSource::new(
            "https://helm.nginx.com/stable",
            "nginx-ingress",
        )),
        VersionedSource::new(data::HelmSource::new(
            "https://gitlab.com/api/v4/projects/29298733/packages/helm/stable",
            "common",
        )),
    ];

    let sources: Vec<Result<_, vigigloo::Error>> =
        futures::future::join_all(sources.into_iter().map(|source| async move {
            let tags = list::list_tags(&source.source, &Default::default()).await?;
            Ok((source, tags))
        }))
        .await;
    let sources = sources.into_iter().filter_map(Result::ok);
    for (source, mut tags) in sources {
        println!(
            "{source} ({loc}):",
            source = source.source.data.describe(),
            loc = source
                .source
                .loc
                .map(|loc| loc.to_string())
                .unwrap_or_else(|| "N/A".to_string()),
        );

        tags.sort_unstable();
        for tag in tags {
            println!("  - {tag}");
        }
    }

    Ok(())
}
