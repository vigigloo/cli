# Changelog

## [1.4.0](https://gitlab.com/vigigloo/cli/-/compare/v1.3.0...v1.4.0) (2023-10-31)

### Features

* Clearer error messages
  ([!68](https://gitlab.com/vigigloo/cli/-/merge_requests/68))([8f5dad40](https://gitlab.com/vigigloo/cli/-/commit/8f5dad40cdc78f2f218c004d7aca25d897abb310))
* **check:** Add support for include/exclude regexes
  ([!65](https://gitlab.com/vigigloo/cli/-/merge_requests/65))([6f0f608](https://gitlab.com/vigigloo/cli/-/commit/6f0f608348368d01f9e23f6bee3333fcee6eaabf))
* **check:** In version check, add an option to don't output if there is 0 new tag
  ([!64](https://gitlab.com/vigigloo/cli/-/merge_requests/64))([4cae883](https://gitlab.com/vigigloo/cli/-/commit/4cae883ee134aae807b61a0abbd98f907fd5488c))
* **check:** Add mail output
  ([!63](https://gitlab.com/vigigloo/cli/-/merge_requests/52))([6e39ad4](https://gitlab.com/vigigloo/cli/-/commit/6e39ad43597ce8e540b12e6c8d6d02e9d676ddfb))
* **check:** Add matrix output
  ([!62](https://gitlab.com/vigigloo/cli/-/merge_requests/52))([6e39ad4](https://gitlab.com/vigigloo/cli/-/commit/6e39ad43597ce8e540b12e6c8d6d02e9d676ddfb))
* **run:** Add action `document`
  ([!60](https://gitlab.com/vigigloo/cli/-/merge_requests/59))([0eed65a](https://gitlab.com/vigigloo/cli/-/commit/0eed65ab29d400067bffbb285cd02c3ddca66515))
* **check:** Handle GitLab and GitHub Docker Registries
  ([!59](https://gitlab.com/vigigloo/cli/-/merge_requests/59))([50c3835](https://gitlab.com/vigigloo/cli/-/commit/50c3835c9f536781d76a0a9f011c9b68f9dfe88f))

### Refactor

* **sources:** Refactor sources handling
  ([!67](https://gitlab.com/vigigloo/cli/-/merge_requests/67))([2349b857](https://gitlab.com/vigigloo/cli/-/commit/2349b857fc6540ea07432c8ae202d04c34361f31))
* **error:** Unify errors type on crate root
  ([!61](https://gitlab.com/vigigloo/cli/-/merge_requests/51))([c7941c7](https://gitlab.com/vigigloo/cli/-/commit/c7941c79bb4729c8e9d7eaa7f7a0cc3943dc4945))

## [1.3.0](https://gitlab.com/vigigloo/cli/compare/v1.2.0...v1.3.0) (2023-08-24)

### Features

* **Outdated/Update:** Add Inline Hints
  ([!53](https://gitlab.com/vigigloo/cli/-/merge_requests/53))([d36615e](https://gitlab.com/vigigloo/cli/-/commit/d36615ec6a1638e85c163bba21c310b7fb90c912))
* **check:** Add Docker images support (only docker.io registry for now)
  ([!52](https://gitlab.com/vigigloo/cli/-/merge_requests/52))([1f55cb4](https://gitlab.com/vigigloo/cli/-/commit/1f55cb4f7811d3978fe03bf5fcba737e25fdeb1b))

## [1.2.0](https://gitlab.com/vigigloo/cli/compare/v1.1.0...v1.2.0) (2023-08-04)

### Features

* **assist:** assist command replaced by run command
  ([!54](https://gitlab.com/vigigloo/cli/-/merge_requests/54))([331d657](https://gitlab.com/vigigloo/cli/-/commit/331d657e0f32e42d2b1cef242eba95e582fb8b11))
* **check:** support npm packages in version check
  ([!49](https://gitlab.com/vigigloo/cli/-/merge_requests/49))([607d230](https://gitlab.com/vigigloo/cli/-/commit/607d2309a51628c195b7099fe79608b6ff710412))
* Environment variables has priority over config file
  ([!47](https://gitlab.com/vigigloo/cli/-/merge_requests/47))([c736b46](https://gitlab.com/vigigloo/cli/-/commit/c736b46ae9b94a3ab713725187e59137cdeb3025))

### Fixes

* **check:** Make `outputs` config field optional
  ([cbe243a](https://gitlab.com/vigigloo/cli/-/commit/cbe243a2a7b81d13d8a4cbaf1cf4fca5210a4c16))
* **update:** Fix update behavior with helm chart when version is exact and
  quoted
  ([!50](https://gitlab.com/vigigloo/cli/-/merge_requests/50))([5290c69](https://gitlab.com/vigigloo/cli/-/commit/5290c69859b2fcfdcbceaf8fe8c45b58ec16e7fc))

* **outdated and update**: recursive on helm charts too + display with right location
  ([!48](https://gitlab.com/vigigloo/cli/-/merge_requests/48))([07e9f29](https://gitlab.com/vigigloo/cli/-/commit/07e9f294e6a50b140494c441a5b56dee594ebf70))
## [1.1.0](https://gitlab.com/vigigloo/cli/-/compare/v1.0.0...v1.1.0) (2023-05-22)

### Features

- **check:** RFC3339 compliance ([!45](https://gitlab.com/vigigloo/cli/-/merge_requests/45))
  ([d5ac2c8](https://gitlab.com/vigigloo/cli/-/commit/d5ac2c8c71d3a4e95b68042b11b914f511d9a4ba))
- **check:** Support sending check notifications to Mattermost ([!39](https://gitlab.com/vigigloo/cli/-/merge_requests/39))
  ([83219874](https://gitlab.com/vigigloo/cli/-/commit/8321987415d4d9b3c8f10ee0bef0ed205780103b))
- **check:** Support Helm Charts in `vigigloo check` ([!44](https://gitlab.com/vigigloo/cli/-/merge_requests/44))
  ([55c55a96](https://gitlab.com/vigigloo/cli/-/commit/55c55a96395b6ada367d42373568e8b25543ad67))
- **assist-codegen:** Add `copy` action ([!37](https://gitlab.com/vigigloo/cli/-/merge_requests/37))
  ([dc6e2dc8](https://gitlab.com/vigigloo/cli/-/commit/dc6e2dc8fb534d79abd1524d4d0ff004b56c8256))
- **assist-codegen:** Add `clean` parameter in `output` action ([!34](https://gitlab.com/vigigloo/cli/-/merge_requests/34))
  ([4c4a1516](https://gitlab.com/vigigloo/cli/-/commit/4c4a1516d1ddc664eb6e7e5842af94034d6f5e52))
- **sources:** Support Helm Charts in `vigigloo outdated` and `vigigloo update` ([!32](https://gitlab.com/vigigloo/cli/-/merge_requests/32))
  ([c4d4c35b](https://gitlab.com/vigigloo/cli/-/commit/c4d4c35b3fd74aa714313c5b942a585faf407b28))

### Fixes

- **sources:** Fix Helm Charts support in `vigigloo outdated` ([!38](https://gitlab.com/vigigloo/cli/-/merge_requests/38))
  ([870ea3bf](https://gitlab.com/vigigloo/cli/-/commit/870ea3bff109fd3833bd8a858b418379b7599fa7))

## [1.0.0](https://gitlab.com/vigigloo/cli/-/commits/v1.0.0) (2023-04-19)

### Features

- **check:** Add command to check new versions in the last X times and output new releases ([!23](https://gitlab.com/vigigloo/cli/-/merge_requests/23))
  ([109e2c](https://gitlab.com/vigigloo/cli/-/commit/109e2c0b2cd98244141d4942b076b8f46a70aadc))
- **update:** Add ability to update out-of-date Terraform providers and modules ([!17](https://gitlab.com/vigigloo/cli/-/merge_requests/17))
  ([7ac839f](https://gitlab.com/vigigloo/cli/-/commit/7ac839f2e3222bafa24518aa8c0016da3aaea5a0))
- **cli:** Add build informations on `--version` flag (for debug purposes)
  ([1d14d4a](https://gitlab.com/vigigloo/cli/-/commit/1d14d4abc1fa3dac820fee0bf86947dba17f03bc))
- **outdated:** Add ability to detect out-of-date Terraform providers and modules ([!8](https://gitlab.com/vigigloo/cli/-/merge_requests/8))
  ([81df38](https://gitlab.com/vigigloo/cli/-/commit/81df38ca43e3244ae654a6eaf68de241a2a48d01))

### Fixes

- **assist:** Correctly deserialize repo sources for `workflow` (Fixes [#39](https://gitlab.com/vigigloo/cli/-/issues/39))
  ([b773fb0](https://gitlab.com/vigigloo/cli/-/commit/b773fb07bd7469c31f0520fcf97f860b8fc82609))
- **rust-toolchain:** Add necessary components for rust build
  ([5dfb0f8](https://gitlab.com/vigigloo/cli/-/commit/5dfb0f895238fe501a1980278a3370b0267cb22c))

## v0.1.0 (2023-02-21)

### Features

- **actions:** Implement `template` action
  ([b015ffd](https://gitlab.com/vigigloo/cli/-/commit/b015ffdc90df57356558b6a5963009fe57c040d7))
- **source:** Support gitlab source
  ([3ab3ff7](https://gitlab.com/vigigloo/cli/-/commit/3ab3ff7fdf068bf8fd50042af02ee68b14a7b080))
- **source:** Fully implement `archive` source
  ([bded27c](https://gitlab.com/vigigloo/cli/-/commit/bded27c193c447933da1c9f816d42e8be961129b))
- **sources:** Add `github` source ability to fetch from archive
  ([7c036f8](https://gitlab.com/vigigloo/cli/-/commit/7c036f882d55884083abbeb37c589b031d414949))
- **action:** Implement `import` action
  ([d1d388a](https://gitlab.com/vigigloo/cli/-/commit/d1d388affd5ff0cc6c6b124a7a3f9644e861d3e3))
- **source:** Implement sources: `github`, `git`
  ([2a3023b](https://gitlab.com/vigigloo/cli/-/commit/2a3023bc30f9c6180d77f74dc71dca36a6feff86))
- **error:** assist and codegen
  ([7a559b2](https://gitlab.com/vigigloo/cli/-/commit/7a559b28e02e9747024d02c4df223991e90e73e1))
- **actions:** error file with custom result for sources
  ([78b425b](https://gitlab.com/vigigloo/cli/-/commit/78b425bac52107c8ec8cb7cccf1f1c8fdee95b84))
- **actions:** errors for actions
  ([d9d71da](https://gitlab.com/vigigloo/cli/-/commit/d9d71da009a26c68091b68b33653544075ee9ed4))
- **actions:** local and output actions
  ([309b6e9](https://gitlab.com/vigigloo/cli/-/commit/309b6e945524fa34e2438f4d77d40bf58c3d3f7a)) | Check output directory before cleaning it ([5f3c99f](https://gitlab.com/vigigloo/cli/commit/5f3c99fc151abd1b352eddb06512d4b42f622621))
- Refactor and add actions
  ([6ab9c4c](https://gitlab.com/vigigloo/cli/-/commit/6ab9c4c0927e99d81984271fba0399aec469f3c7))
- Add logging
  ([d4882d7](https://gitlab.com/vigigloo/cli/-/commit/d4882d779f39fb53872efb12aa4c6273ee0dc371))
- Update codegen config parser
  ([8e4ece9](https://gitlab.com/vigigloo/cli/-/commit/8e4ece90fff49536dee117e32ddba569e3814fee))
- Initial commit.
  ([6f71f48](https://gitlab.com/vigigloo/cli/-/commit/6f71f489c36f80f19b4f1666ec9da792d38934f7))
