ARG RUST_CHEF_IMAGE=docker.io/lukemathwalker/cargo-chef:latest-rust-latest
ARG RUNTIME_FROM=gcr.io/distroless/cc-debian12

FROM $RUST_CHEF_IMAGE AS chef
WORKDIR /app

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef AS builder
ARG LIBGCC_VERSION=12
RUN apt-get update && apt-get install -y ca-certificates libz-dev libgcc-${LIBGCC_VERSION}-dev libssl-dev
COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --features cli --recipe-path recipe.json
COPY . .
RUN cargo build --release --features cli --bin vigigloo

FROM $RUNTIME_FROM
ARG TARGETOS
ARG LIB_ARCH=x86_64

COPY --from=builder /etc/ssl/certs /etc/ssl/certs

# List from https://packages.debian.org/search?searchon=contents&keywords=libz.so
# List from https://packages.debian.org/search?searchon=contents&keywords=libgcc_s.so
# List from https://packages.debian.org/search?searchon=contents&keywords=libssl.so
COPY --from=builder \
    /usr/lib/${LIB_ARCH}-${TARGETOS}-gnu/libz* \
    /usr/lib/${LIB_ARCH}-${TARGETOS}-gnu/libgcc* \
    /usr/lib/${LIB_ARCH}-${TARGETOS}-gnu/libssl* \
    /usr/lib/${LIB_ARCH}-${TARGETOS}-gnu/libcrypto* \
    /usr/lib/${LIB_ARCH}-${TARGETOS}-gnu/

COPY --from=builder /app/target/release/vigigloo /usr/local/bin/

CMD ["/usr/local/bin/vigigloo"]
