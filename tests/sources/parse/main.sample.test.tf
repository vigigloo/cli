terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.8"
    }
  }
  required_version = ">= 0.14"
}

module "namespace-versioned" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.3.5"
}

module "namespace-unversioned" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
}

module "mattermost" {
  source  = "gitlab.com/vigigloo/tools-k8s/mattermost"
  version = "0.1.0"

  error_case    = "foo"
  # @vigigloo not_a_key_word "helm+https://helm.nginx.com/stable#name=nginx-ingress" "0.1.0
  error_version = "0.1.0"

  chart_name    = "nginx-ingress"
  # @vigigloo source "helm+https://helm.nginx.com/stable#name=nginx-ingress" "> 0.10.0 , < 0.18.0"
  chart_version = "0.17.1"

  image_repository = "mattermost/mattermost-team-edition"
  # @vigigloo source "terraform://gitlab.com/vigigloo/tf-modules/k8slimitednamespace" "2.0.0"
  image_tag        = "2.0.0"

}
