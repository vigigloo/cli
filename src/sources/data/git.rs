use crate::Result;

#[derive(Debug, Clone, PartialEq, serde::Deserialize)]
pub struct GitSource(pub url::Url);

impl GitSource {
    pub fn new<Url>(url: Url) -> Result<Self>
    where
        Url: TryInto<url::Url>,
        crate::Error: From<Url::Error>,
    {
        Ok(Self(url.try_into()?))
    }

    pub fn url(&self) -> &url::Url {
        &self.0
    }

    pub fn describe(&self) -> String {
        self.url().to_string()
    }
}
