#[derive(Debug, Clone, PartialEq, serde::Deserialize)]
pub struct DockerSource(pub url::Url);

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Registry {
    Gitlab,
    Github,
    DockerHub,
}

impl DockerSource {
    pub fn url(&self) -> &url::Url {
        &self.0
    }

    pub fn registry(&self) -> Option<Registry> {
        use crate::util::url::UrlExt;

        match (self.url().host_str(), self.url().scheme_secondary()) {
            (Some("gitlab.com"), _) | (_, Some("gitlab")) => Some(Registry::Gitlab),
            (Some("ghcr.io"), _) => Some(Registry::Github),
            (Some("docker.io"), _) | (_, None) => Some(Registry::DockerHub),
            _ => None,
        }
    }

    pub fn describe(&self) -> String {
        self.url().to_string()
    }
}
