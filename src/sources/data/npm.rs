#[derive(Debug, Clone, PartialEq, serde::Deserialize)]
pub struct NpmSource(pub url::Url);

impl NpmSource {
    pub fn url(&self) -> &url::Url {
        &self.0
    }

    pub fn describe(&self) -> String {
        self.url().to_string()
    }
}
