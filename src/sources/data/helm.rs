use crate::{Error, Result};

#[derive(Debug, Clone, PartialEq, serde::Deserialize)]
pub struct HelmSource {
    pub repository: String,
    pub name: String,
}

impl HelmSource {
    pub fn new(repository: impl Into<String>, name: impl Into<String>) -> Self {
        Self {
            repository: repository.into(),
            name: name.into(),
        }
    }

    pub fn repository(&self) -> &str {
        &self.repository
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn describe(&self) -> String {
        self.name.clone()
    }

    pub fn parse_url(url: &url::Url) -> Result<Self> {
        #[derive(Debug, serde::Deserialize)]
        struct HelmChartUrlFragment {
            name: String,
        }

        let mut repository = url.clone();
        repository.set_fragment(None);

        let repository = repository
            .as_str()
            .strip_prefix("helm+")
            .unwrap()
            .to_string();

        let full_fragment = url
            .fragment()
            .ok_or(Error::URLFragmentMissing(url.clone()))?;
        let meta: HelmChartUrlFragment = serde_qs::from_str::<HelmChartUrlFragment>(full_fragment)
            .map_err(|err| {
                Error::serde_qs(
                    err,
                    format!(
                        "The URL fragment '{}' is invalid for repository '{}'",
                        full_fragment, repository
                    ),
                )
            })?;

        Ok(Self {
            repository,
            name: meta.name,
        })
    }
}
