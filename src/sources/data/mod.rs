use std::fmt;

pub use self::{
    docker::DockerSource,
    git::GitSource,
    helm::HelmSource,
    npm::NpmSource,
    terraform::{TerraformSource, TerraformSourceKind},
};
use crate::{Error, Result};

mod docker;
mod git;
mod helm;
mod npm;
mod terraform;

#[serde_with::serde_as]
#[derive(Debug, Clone, PartialEq, serde::Deserialize)]
pub enum Source {
    Docker(DockerSource),
    Git(GitSource),
    Npm(NpmSource),
    Terraform(
        #[serde_as(as = "serde_with::FromInto<terraform::TerraformSourceSerialized>")]
        TerraformSource,
    ),
    Helm(HelmSource),
}

enum_from!(Source {
    DockerSource => Docker,
    GitSource => Git,
    NpmSource => Npm,
    TerraformSource => Terraform,
    HelmSource => Helm,
});

impl Source {
    pub fn describe(&self) -> String {
        match self {
            Source::Docker(source) => source.describe(),
            Source::Git(source) => source.describe(),
            Source::Npm(source) => source.describe(),
            Source::Terraform(source) => source.describe(),
            Source::Helm(source) => source.describe(),
        }
    }

    pub fn parse_url(url: &url::Url) -> Result<Self> {
        use crate::util::url::UrlExt;

        if url.scheme_main() == "docker" {
            Ok(DockerSource(url.clone()).into())
        } else if url.scheme_main() == "git" {
            Ok(GitSource(url.clone()).into())
        } else if url.scheme() == "terraform" {
            Ok(TerraformSource::parse_url(url).into())
        } else if url.scheme().starts_with("helm") {
            Ok(HelmSource::parse_url(url)?.into())
        } else {
            Err(Error::UnknownSourceType(url.clone()))
        }
    }
}

impl fmt::Display for Source {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Source::Docker(_) => f.write_str("Docker image"),
            Source::Git(_) => f.write_str("Git repository"),
            Source::Npm(_) => f.write_str("NPM package"),
            Source::Helm(_) => f.write_str("Helm Chart"),
            Source::Terraform(source) => source.fmt(f),
        }
    }
}
