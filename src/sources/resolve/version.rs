use crate::sources::data::Source;
use crate::Result;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Version {
    SemVer(semver::Version),
    Terraform(terraform_version::Version),
}

impl Version {
    pub fn parse(source: &Source, version: &str) -> Result<Self> {
        Ok(match source {
            Source::Terraform(_) => Version::Terraform(terraform_version::Version::parse(version)?),
            _ => Version::SemVer(semver::Version::parse(version)?),
        })
    }
}

#[derive(Debug, Clone)]
pub enum VersionReq {
    SemVer(semver::VersionReq),
    Terraform(terraform_version::VersionRequirement),
}

impl VersionReq {
    pub fn parse(source: &Source, req: &str) -> Result<Self> {
        Ok(match source {
            Source::Terraform(_) => {
                VersionReq::Terraform(terraform_version::VersionRequirement::parse(req)?)
            }
            _ => VersionReq::SemVer(semver::VersionReq::parse(req)?),
        })
    }

    pub fn matches(&self, version: &Version) -> bool {
        match (self, version) {
            (VersionReq::SemVer(req), Version::SemVer(version)) => req.matches(version),
            (VersionReq::Terraform(req), Version::Terraform(version)) => version.matches(req),
            _ => unreachable!(),
        }
    }
}
