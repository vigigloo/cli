use core::ops::Deref;

use crate::error::ErrorWithData;

pub mod data;
pub mod fetch;
pub mod list;
pub mod parse;
pub mod resolve;

#[derive(Debug, Clone, PartialEq)]
pub struct Source {
    pub data: data::Source,
    pub loc: Option<parse::Location>,
}

impl Source {
    pub fn new(data: data::Source) -> Self {
        Self { data, loc: None }
    }

    pub fn location(&self) -> Option<&parse::Location> {
        self.loc.as_ref()
    }

    pub fn with_location(self, loc: parse::Location) -> Self {
        Self {
            loc: Some(loc),
            ..self
        }
    }
}

impl Deref for Source {
    type Target = data::Source;

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl<T: Into<data::Source>> From<T> for Source {
    fn from(value: T) -> Self {
        Self::new(value.into())
    }
}

pub type Version = String;
pub type VersionReq = String;

#[derive(Debug, Clone, PartialEq)]
pub struct VersionedSource {
    pub source: Source,
    pub current: Option<VersionReq>,
    pub required: Option<VersionReq>,
}

impl VersionedSource {
    pub fn new<T: Into<Source>>(source: T) -> Self {
        Self {
            source: source.into(),
            current: None,
            required: None,
        }
    }

    pub fn with_current(self, current: impl Into<VersionReq>) -> Self {
        Self {
            current: Some(current.into()),
            ..self
        }
    }

    pub fn with_required(self, required: impl Into<VersionReq>) -> Self {
        Self {
            required: Some(required.into()),
            ..self
        }
    }

    pub fn with_location(self, loc: parse::Location) -> Self {
        Self {
            source: self.source.with_location(loc),
            ..self
        }
    }
}

pub fn print_errors(errors: Vec<ErrorWithData<VersionedSource>>) {
    for (err, source) in errors.into_iter().map(ErrorWithData::split) {
        let loc = source.source.location().unwrap();

        println!(
            "Error at `{}`:\n\t{:?}\n\t\t{:?}",
            loc,
            source.source.describe(),
            miette::Report::new(err)
        )
    }
}
