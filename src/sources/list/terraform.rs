use super::Tag;
use crate::sources::data::{TerraformSource, TerraformSourceKind};
use crate::Result;

pub async fn list_tags(source: &TerraformSource) -> Result<Vec<Tag>> {
    use terraform_registry::Address;

    let address = match source.kind {
        TerraformSourceKind::Module => Address::parse_module(&source.address)?,
        TerraformSourceKind::Provider => Address::parse_provider(&source.address)?,
    };

    let client = address.connect_registry().await?;

    Ok(match source.kind {
        TerraformSourceKind::Module => client
            .list_module_versions(&address)
            .await?
            .into_iter()
            .map(|module| Tag::new_without_date(module.version))
            .collect::<Vec<_>>(),
        TerraformSourceKind::Provider => client
            .list_provider_versions(&address)
            .await?
            .into_iter()
            .map(|provider| Tag::new_without_date(provider.version))
            .collect::<Vec<_>>(),
    })
}
