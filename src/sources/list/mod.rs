use std::fmt;

use chrono::{DateTime, Utc};
use smart_default::SmartDefault;

use super::Source;
use crate::util::iter::FilterWhileAction;
use crate::Result;

mod docker;
mod git;
mod helm;
mod npm;
mod terraform;

#[derive(Debug, Clone, serde::Serialize)]
pub struct Tag {
    name: String,
    created_at: Option<chrono::DateTime<chrono::Utc>>,
}

impl Tag {
    pub fn new(
        name: impl Into<String>,
        created_at: impl Into<chrono::DateTime<chrono::Utc>>,
    ) -> Self {
        Self {
            name: name.into(),
            created_at: Some(created_at.into()),
        }
    }

    pub fn new_without_date(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            created_at: None,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn created_at(&self) -> Option<&chrono::DateTime<chrono::Utc>> {
        self.created_at.as_ref()
    }

    pub fn with_created_at(self, created_at: impl Into<chrono::DateTime<chrono::Utc>>) -> Self {
        Self {
            created_at: Some(created_at.into()),
            ..self
        }
    }
}

impl PartialEq for Tag {
    fn eq(&self, other: &Self) -> bool {
        self.name() == other.name()
    }
}

impl Eq for Tag {}

impl PartialOrd for Tag {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Tag {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.name.cmp(&other.name)
    }
}

impl fmt::Display for Tag {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{name}", name = self.name)?;
        if let Some(created_at) = self.created_at {
            write!(f, " ({created_at})")?;
        }
        Ok(())
    }
}

#[derive(SmartDefault)]
pub struct ListOptions<'a> {
    since: Option<DateTime<Utc>>,
    #[allow(clippy::type_complexity)]
    filter_fn: Option<Box<dyn Fn(&str, &DateTime<Utc>) -> FilterWhileAction + 'a>>,
}

impl<'a> ListOptions<'a> {
    #[inline]
    pub fn new() -> Self {
        Self::default()
    }

    pub fn cloned<'b>(&'a self) -> ListOptions<'b>
    where
        'a: 'b,
    {
        let mut options = Self {
            since: self.since,
            ..Default::default()
        };

        if let Some(ref filter_fn) = self.filter_fn {
            options = options.filter(filter_fn);
        }

        options
    }

    pub fn since(mut self, since: DateTime<Utc>) -> Self {
        self.since = Some(since);
        self
    }

    pub fn filter(
        mut self,
        filter_fn: impl Fn(&str, &DateTime<Utc>) -> FilterWhileAction + 'a,
    ) -> Self {
        self.filter_fn = Some(Box::new(filter_fn));
        self
    }

    pub fn add_filter(
        mut self,
        filter_fn: impl Fn(FilterWhileAction, &str, &DateTime<Utc>) -> FilterWhileAction + 'a,
    ) -> Self {
        let current_filter = self.filter_fn.take();
        self.filter(move |name, date| {
            let lhs = current_filter
                .as_ref()
                .map(|filter_fn| filter_fn(name, date))
                .unwrap_or_default();

            filter_fn(lhs, name, date)
        })
    }

    pub fn skip_when_true(
        self,
        filter_fn: impl Fn(FilterWhileAction, &str, &DateTime<Utc>) -> bool + 'a,
    ) -> Self {
        self.add_filter(move |action, name, created_at| {
            if filter_fn(action, name, created_at) {
                FilterWhileAction::Pick
            } else {
                FilterWhileAction::Skip
            }
        })
    }
}

pub async fn list_tags(source: &Source, options: &ListOptions<'_>) -> Result<Vec<Tag>> {
    use crate::sources::data::Source as SourceBase;

    match source.data {
        SourceBase::Docker(ref source) => docker::list_tags(source, options).await,
        SourceBase::Git(ref source) => git::list_tags(source, options).await,
        SourceBase::Helm(ref source) => helm::list_tags(source).await,
        SourceBase::Npm(ref source) => npm::list_tags(source).await,
        SourceBase::Terraform(ref source) => terraform::list_tags(source).await,
    }
}
