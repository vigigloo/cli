use super::{ListOptions, Tag};
use crate::{Error, Result};

#[derive(Debug, serde::Serialize)]
struct Params {
    page: u32,
    per_page: u32,
    state: State,
}

#[derive(Debug, serde::Serialize)]
#[serde(rename_all = "lowercase")]
enum State {
    Active,
    #[allow(dead_code)]
    Deleted,
}

#[derive(Debug, serde::Deserialize)]
struct Entry {
    name: String,
    created_at: chrono::DateTime<chrono::Utc>,
    metadata: Metadata,
}

#[derive(Debug, serde::Deserialize)]
struct Metadata {
    container: ContainerMetadata,
}

#[derive(Debug, serde::Deserialize)]
struct ContainerMetadata {
    tags: Vec<String>,
}

pub async fn fetch(source: &url::Url, options: &ListOptions<'_>) -> Result<Vec<Tag>> {
    let Some((org, name)) = source.path().trim_start_matches('/').split_once('/') else {
        return Err(Error::invalid_source(
            source.clone(),
            "Invalid path (expected format: <ORG>/<NAME>)",
        ));
    };

    let builder = octocrab::Octocrab::builder();

    let builder = if let Ok(token) = std::env::var("VIGIGLOO_GITHUB_TOKEN") {
        builder.personal_token(token)
    } else {
        builder
    };

    let client = builder
        .build()
        .map_err(|err| Error::github(err, "Could not build GitHub client"))?;

    let mut params = Params {
        page: 1,
        per_page: 100,
        state: State::Active,
    };

    let route = format!("/orgs/{org}/packages/container/{name}/versions");

    let mut tags = Vec::new();

    'tags: loop {
        let data: Vec<Entry> = client
            .get(&route, Some(&params))
            .await
            .map_err(|err| Error::github(err, "Could not fetch packages page"))?;

        if data.is_empty() {
            break;
        }

        let entries = data.iter().flat_map(|entry| {
            entry
                .metadata
                .container
                .tags
                .iter()
                .map(move |tag| (entry, tag))
        });
        for (entry, name) in entries {
            use crate::util::iter::FilterWhileAction;

            if let Some(since) = options.since {
                if entry.created_at < since {
                    break 'tags;
                }
            }

            let action = options
                .filter_fn
                .as_ref()
                .map(|filter_fn| filter_fn(&entry.name, &entry.created_at))
                .unwrap_or(FilterWhileAction::Pick);
            match action {
                FilterWhileAction::Pick => tags.push(Tag::new(name, entry.created_at)),
                FilterWhileAction::Skip => continue,
                FilterWhileAction::Break => break 'tags,
            }
        }

        params.page += 1;
    }

    Ok(tags)
}
