use chrono::{DateTime, Utc};
use serde::Deserialize;

use super::{ListOptions, Tag};
use crate::Result;

#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
struct TagsPage {
    count: u32,
    next: Option<url::Url>,
    #[serde_as(as = "serde_with::VecSkipError<_>")]
    results: Vec<TagsPageTag>,
}

#[derive(Debug, Deserialize)]
struct TagsPageTag {
    name: String,
    last_updated: DateTime<Utc>,
}

pub async fn fetch(source: &url::Url, options: &ListOptions<'_>) -> Result<Vec<Tag>> {
    let path = match source.host_str() {
        Some("docker.io") | None => source.path().trim_start_matches('/').to_owned(),
        Some(org) => format!("{org}{path}", path = source.path()),
    };
    let (namespace, repository) = match path.split_once('/') {
        Some(pair) => pair,
        None => ("library", path.as_str()),
    };

    let client = reqwest::Client::builder().https_only(true).build()?;
    let mut url: url::Url =
        format!("https://hub.docker.com/v2/namespaces/{namespace}/repositories/{repository}/tags?page_size=100")
            .parse()
            .unwrap();

    let first: TagsPage = client.get(url.as_str()).send().await?.json().await?;

    let mut tags = Vec::with_capacity(first.count as usize);

    'tags: loop {
        let page: TagsPage = client.get(url.as_str()).send().await?.json().await?;

        url = match page.next {
            Some(next) => next,
            None => break,
        };

        for item in page.results {
            use crate::util::iter::FilterWhileAction;

            if let Some(since) = options.since {
                if item.last_updated < since {
                    break 'tags;
                }
            }

            let action = options
                .filter_fn
                .as_ref()
                .map(|filter_fn| filter_fn(&item.name, &item.last_updated))
                .unwrap_or(FilterWhileAction::Pick);
            match action {
                FilterWhileAction::Pick => tags.push(Tag::new(item.name, item.last_updated)),
                FilterWhileAction::Skip => continue,
                FilterWhileAction::Break => break 'tags,
            }
        }
    }

    Ok(tags)
}
