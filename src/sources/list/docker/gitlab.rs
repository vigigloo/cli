use super::{ListOptions, Tag};
use crate::Result;

mod queries {
    pub use self::list_tags::{get_project_root_repository, GetProjectRootRepository};
    pub use self::list_tags::{list_container_tags, ListContainerTags};

    mod list_tags;
}

pub async fn fetch(source: &url::Url, options: &ListOptions<'_>) -> Result<Vec<Tag>> {
    use graphql_client::reqwest as graphql;

    use queries::{GetProjectRootRepository, ListContainerTags};

    let host = source.host_str().expect("No GitLab host given");
    let path = source.path().trim_start_matches('/');

    let client = reqwest::Client::builder().build()?;

    let data = graphql::post_graphql::<GetProjectRootRepository, _>(
        &client,
        format!("https://{host}/api/graphql"),
        queries::get_project_root_repository::Variables {
            path: path.to_string(),
        },
    )
    .await?
    .data
    .expect("No data");

    let project = data.project.expect("Project does not exists");
    let id = project.container_repositories.nodes[0].id.as_str();

    let mut cursor = None;
    let mut tags = Vec::new();

    'tags: loop {
        let data = graphql::post_graphql::<ListContainerTags, _>(
            &client,
            format!("https://{host}/api/graphql"),
            queries::list_container_tags::Variables {
                container_id: id.to_string(),
                after: cursor.clone(),
            },
        )
        .await?
        .data
        .expect("No data");

        let repository_tags = data.container_repository.tags;

        cursor = repository_tags.page_info.end_cursor;
        if cursor.is_none() {
            break;
        }

        for node in repository_tags.nodes {
            use crate::util::iter::FilterWhileAction;

            if let Some(since) = options.since {
                if node.created_at < since {
                    break 'tags;
                }
            }

            let action = options
                .filter_fn
                .as_ref()
                .map(|filter_fn| filter_fn(&node.name, &node.created_at))
                .unwrap_or(FilterWhileAction::Pick);
            match action {
                FilterWhileAction::Pick => tags.push(Tag::new(node.name, node.created_at)),
                FilterWhileAction::Skip => continue,
                FilterWhileAction::Break => break 'tags,
            }
        }
    }

    Ok(tags)
}
