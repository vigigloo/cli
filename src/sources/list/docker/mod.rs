use super::{ListOptions, Tag};
use crate::sources::data::DockerSource;
use crate::{Error, Result};

mod docker_io;
mod github;
mod gitlab;

pub async fn list_tags(source: &DockerSource, options: &ListOptions<'_>) -> Result<Vec<Tag>> {
    use crate::util::url::UrlExt;

    let url = source.url();

    match (url.host_str(), url.scheme_secondary()) {
        (Some("gitlab.com"), _) | (_, Some("gitlab")) => gitlab::fetch(url, options).await,
        (Some("ghcr.io"), _) => github::fetch(url, options).await,
        (Some("docker.io"), _) | (_, None) => docker_io::fetch(url, options).await,
        _ => Err(Error::UnsupportedDockerRegistry(url.clone())),
    }
}
