// Generated using graphql-client CLI

pub const QUERY: &str = include_str!("list-tags.graphql");

pub mod get_project_root_repository {
    use serde::{Deserialize, Serialize};

    pub const OPERATION_NAME: &str = "GetProjectRootRepository";

    type ID = String;

    #[derive(Debug, Serialize)]
    pub struct Variables {
        pub path: ID,
    }

    #[derive(Debug, Deserialize)]
    pub struct ResponseData {
        pub project: Option<GetProjectRootRepositoryProject>,
    }
    #[derive(Debug, Deserialize)]
    pub struct GetProjectRootRepositoryProject {
        #[serde(rename = "containerRepositories")]
        pub container_repositories: GetProjectRootRepositoryProjectContainerRepositories,
    }
    #[derive(Debug, Deserialize)]
    pub struct GetProjectRootRepositoryProjectContainerRepositories {
        pub nodes: Vec<GetProjectRootRepositoryProjectContainerRepositoriesNodes>,
    }
    #[derive(Debug, Deserialize)]
    pub struct GetProjectRootRepositoryProjectContainerRepositoriesNodes {
        pub id: ID,
    }
}

pub struct GetProjectRootRepository;

impl graphql_client::GraphQLQuery for GetProjectRootRepository {
    type Variables = get_project_root_repository::Variables;
    type ResponseData = get_project_root_repository::ResponseData;

    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: QUERY,
            operation_name: get_project_root_repository::OPERATION_NAME,
        }
    }
}

pub mod list_container_tags {
    use serde::{Deserialize, Serialize};

    pub const OPERATION_NAME: &str = "ListContainerTags";

    type ContainerRepositoryID = String;
    type Time = chrono::DateTime<chrono::Utc>;

    #[derive(Debug, Serialize)]
    pub struct Variables {
        #[serde(rename = "containerID")]
        pub container_id: ContainerRepositoryID,
        pub after: Option<String>,
    }

    #[derive(Debug, Deserialize)]
    pub struct ResponseData {
        #[serde(rename = "containerRepository")]
        pub container_repository: ListContainerTagsContainerRepository,
    }
    #[derive(Debug, Deserialize)]
    pub struct ListContainerTagsContainerRepository {
        pub tags: ListContainerTagsContainerRepositoryTags,
    }
    #[derive(Debug, Deserialize)]
    pub struct ListContainerTagsContainerRepositoryTags {
        #[serde(rename = "pageInfo")]
        pub page_info: ListContainerTagsContainerRepositoryTagsPageInfo,
        pub nodes: Vec<ListContainerTagsContainerRepositoryTagsNodes>,
    }
    #[derive(Debug, Deserialize)]
    pub struct ListContainerTagsContainerRepositoryTagsPageInfo {
        #[serde(rename = "endCursor")]
        pub end_cursor: Option<String>,
    }
    #[derive(Debug, Deserialize)]
    pub struct ListContainerTagsContainerRepositoryTagsNodes {
        pub name: String,
        #[serde(rename = "createdAt")]
        pub created_at: Time,
    }
}

pub struct ListContainerTags;

impl graphql_client::GraphQLQuery for ListContainerTags {
    type Variables = list_container_tags::Variables;
    type ResponseData = list_container_tags::ResponseData;

    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: QUERY,
            operation_name: list_container_tags::OPERATION_NAME,
        }
    }
}
