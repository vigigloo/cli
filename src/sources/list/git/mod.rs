use super::{ListOptions, Tag};
use crate::sources::data::GitSource;
use crate::Result;

mod github;
mod gitlab;
mod raw;

pub async fn list_tags(source: &GitSource, options: &ListOptions<'_>) -> Result<Vec<Tag>> {
    match source.url().host_str() {
        Some("github.com") => github::list_tags(source.url(), options).await,
        Some("gitlab.com") => gitlab::list_tags(source.url(), options).await,
        _ => raw::list_tags(source.url(), options).await,
    }
}
