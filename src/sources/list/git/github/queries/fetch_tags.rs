// Generated using graphql-client CLI

pub const QUERY: &str = include_str!("fetch-tags.graphql");

pub mod fetch_tags_query {
    use serde::{Deserialize, Serialize};

    pub const OPERATION_NAME: &str = "FetchTagsQuery";

    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    type DateTime = chrono::DateTime<chrono::Utc>;
    type GitTimestamp = chrono::DateTime<chrono::Utc>;

    #[derive(Serialize)]
    pub struct Variables {
        pub owner: String,
        pub name: String,
        pub after: Option<String>,
    }
    impl Variables {}
    #[derive(Deserialize)]
    pub struct ResponseData {
        pub repository: FetchTagsQueryRepository,
    }
    #[derive(Deserialize)]
    pub struct FetchTagsQueryRepository {
        pub refs: FetchTagsQueryRepositoryRefs,
    }
    #[serde_with::serde_as]
    #[derive(Deserialize)]
    pub struct FetchTagsQueryRepositoryRefs {
        #[serde(rename = "pageInfo")]
        pub page_info: FetchTagsQueryRepositoryRefsPageInfo,
        #[serde(default)]
        #[serde_as(deserialize_as = "serde_with::DefaultOnNull")]
        pub nodes: Vec<FetchTagsQueryRepositoryRefsNodes>,
    }
    #[derive(Deserialize)]
    pub struct FetchTagsQueryRepositoryRefsPageInfo {
        #[serde(rename = "endCursor")]
        pub end_cursor: Option<String>,
    }
    #[derive(Deserialize)]
    pub struct FetchTagsQueryRepositoryRefsNodes {
        pub name: String,
        pub target: FetchTagsQueryRepositoryRefsNodesTarget,
    }
    #[derive(Deserialize)]
    #[serde(tag = "__typename")]
    pub enum FetchTagsQueryRepositoryRefsNodesTarget {
        Blob,
        Commit(FetchTagsQueryRepositoryRefsNodesTargetOnCommit),
        Tag(FetchTagsQueryRepositoryRefsNodesTargetOnTag),
        Tree,
    }
    #[derive(Deserialize)]
    pub struct FetchTagsQueryRepositoryRefsNodesTargetOnCommit {
        #[serde(rename = "commitDate")]
        pub commit_date: DateTime,
    }
    #[derive(Deserialize)]
    pub struct FetchTagsQueryRepositoryRefsNodesTargetOnTag {
        pub tagger: FetchTagsQueryRepositoryRefsNodesTargetOnTagTagger,
    }
    #[derive(Deserialize)]
    pub struct FetchTagsQueryRepositoryRefsNodesTargetOnTagTagger {
        #[serde(rename = "tagDate")]
        pub tag_date: GitTimestamp,
    }
}

pub struct FetchTagsQuery;

impl graphql_client::GraphQLQuery for FetchTagsQuery {
    type Variables = fetch_tags_query::Variables;
    type ResponseData = fetch_tags_query::ResponseData;

    fn build_query(variables: Self::Variables) -> graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: QUERY,
            operation_name: fetch_tags_query::OPERATION_NAME,
        }
    }
}
