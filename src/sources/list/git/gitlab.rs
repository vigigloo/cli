use super::{ListOptions, Tag};
use crate::{Error, Result};

pub async fn list_tags(source: &url::Url, _options: &ListOptions<'_>) -> Result<Vec<Tag>> {
    use gitlab::{
        api::{projects::repository::tags, AsyncQuery},
        types::Tag as GitlabTag,
        GitlabBuilder,
    };

    let host = source
        .host_str()
        .ok_or_else(|| Error::invalid_source(source.clone(), "No host provided"))?;

    let path = source.path().strip_prefix('/').unwrap_or(source.path());

    let client = GitlabBuilder::new_unauthenticated(host)
        .build_async()
        .await
        .map_err(|err| Error::gitlab(err, "Could not connect to GitLab"))?;

    let gitlab_tags: Vec<GitlabTag> = tags::Tags::builder()
        .project(path)
        .build()
        .unwrap() // Should not fail since all builder fields are filled
        .query_async(&client)
        .await
        .map_err(|err| Error::gitlab(err, "Could not fetch project tags"))?;

    let tags = gitlab_tags
        .into_iter()
        .map(|tag| Tag::new(tag.name, tag.commit.created_at))
        .collect();

    Ok(tags)
}
