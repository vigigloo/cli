use super::{ListOptions, Tag};
use crate::{Error, Result};

mod queries {
    pub use self::fetch_tags::{fetch_tags_query, FetchTagsQuery};

    mod fetch_tags;
}

pub async fn list_tags(source: &url::Url, options: &ListOptions<'_>) -> Result<Vec<Tag>> {
    let Some((owner, repo)) = source.path().trim_start_matches('/').split_once('/') else {
        return Err(Error::invalid_source(
            source.clone(),
            "Invalid path (expected format <OWNER>/<REPO>)",
        ));
    };

    let github_token = std::env::var("VIGIGLOO_GITHUB_TOKEN").map_err(|err| {
        Error::env(
            err,
            "VIGIGLOO_GITHUB_TOKEN",
            "A GitHub token is needed for GitHub sources",
        )
    })?;

    let client = octocrab::Octocrab::builder()
        .personal_token(github_token)
        .build()
        .map_err(|err| Error::github(err, "Could not build GitHub client"))?;

    let mut cursor = None;
    let mut tags = Vec::new();

    'tags: loop {
        use graphql_client::{GraphQLQuery, Response};

        use queries::{
            fetch_tags_query::{ResponseData, Variables},
            FetchTagsQuery,
        };

        let query = FetchTagsQuery::build_query(Variables {
            owner: owner.into(),
            name: repo.into(),
            after: cursor.clone(),
        });
        let res: Response<ResponseData> = client.graphql(&query).await.map_err(|err| {
            Error::github(
                err,
                format!("Could not fetch repository tags from {:?}", source.as_str()),
            )
        })?;
        let data = res.data.expect("No data");
        let refs = data.repository.refs;

        cursor = refs.page_info.end_cursor;

        if cursor.is_none() {
            break;
        }

        let nodes = refs.nodes;
        for node in nodes {
            use crate::util::iter::FilterWhileAction;
            use queries::fetch_tags_query::FetchTagsQueryRepositoryRefsNodesTarget as Target;

            let name = node.name;

            let target = node.target;
            let date = match target {
                Target::Commit(data) => data.commit_date,
                Target::Tag(data) => data.tagger.tag_date,
                _ => continue,
            };

            if let Some(since) = options.since {
                if date < since {
                    break 'tags;
                }
            }

            let action = options
                .filter_fn
                .as_ref()
                .map(|filter_fn| filter_fn(&name, &date))
                .unwrap_or(FilterWhileAction::Pick);
            match action {
                FilterWhileAction::Pick => tags.push(Tag::new(name, date)),
                FilterWhileAction::Skip => continue,
                FilterWhileAction::Break => break 'tags,
            }
        }
    }

    Ok(tags)
}
