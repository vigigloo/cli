use serde::Deserialize;
use std::collections::HashMap;

use super::Tag;
use crate::sources::data::HelmSource;
use crate::{Error, Result};

#[derive(Debug, Clone, Deserialize)]
struct Index {
    entries: HashMap<String, Vec<Entry>>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Entry {
    pub version: String,
    pub created: chrono::DateTime<chrono::Utc>,
}

pub async fn fetch_entries(repository: &str, name: &str) -> Result<Vec<Entry>> {
    let index = format!("{repository}/index.yaml");

    let res = reqwest::get(index).await?;
    let content = res.text().await?;
    let data: Index = serde_yaml::from_str(&content)?;

    Ok(data
        .entries
        .get(name)
        .ok_or(Error::HelmEntryError(
            name.to_string(),
            repository.to_string(),
        ))?
        .clone())
}

pub async fn list_tags(source: &HelmSource) -> Result<Vec<Tag>> {
    let entries = fetch_entries(&source.repository, &source.name).await?;

    Ok(entries
        .into_iter()
        .map(|entry| Tag::new(entry.version, entry.created))
        .collect())
}
