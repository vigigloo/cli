use std::path::Path;

use url::Url;

use super::ParseOptions;
use crate::parsers::hcl::ast::{Attribute, InlineHint};
use crate::sources::VersionedSource;
use crate::{Error, Result};

pub async fn parse_dir(
    path: impl AsRef<Path>,
    opts: &ParseOptions,
) -> impl IntoIterator<Item = Result<VersionedSource>> {
    use futures::future::join_all;
    use itertools::Itertools;

    let pattern = if opts.recursive { "**/*.tf" } else { "*.tf" };
    let glob = wax::Glob::new(pattern).unwrap();

    let walker = glob
        .walk(path)
        .filter_map(|entry| entry.ok())
        .filter(|entry| opts.hidden || !entry.to_candidate_path().as_ref().starts_with('.'))
        .map(parse);

    join_all(walker)
        .await
        .into_iter()
        .flatten_ok()
        .map(|res| res.and_then(std::convert::identity))
}

async fn parse(entry: wax::WalkEntry<'_>) -> Result<Vec<Result<VersionedSource>>> {
    use tokio::fs;

    use crate::parsers::hcl as parser;
    use crate::sources::data::{Source as BaseSource, TerraformSource, TerraformSourceKind};
    use crate::util::hcl;

    let content = fs::read_to_string(entry.path())
        .await
        .map_err(|err| Error::io(err, "Failed to read Terraform file"))?;
    let (ast, ast_errors) = parser::parse(&content)?;

    let mut sources = Vec::new();

    // parsing errors copy
    for e in ast_errors {
        sources.push(Err(Error::HclParsing {
            file: entry.path().to_str().unwrap().to_string(),
            err: e,
        }));
    }

    // Parse providers
    if let Some(terraform) = ast.get_block("terraform", None) {
        if let Some(required_providers) = terraform.get_block("required_providers", None) {
            let providers = required_providers
                .attributes
                .iter()
                .filter_map(|attr| attr.expr.as_object().map(|obj| (attr.name, obj)));
            for (_name, obj) in providers {
                let address = obj
                    .get_by_name("source")
                    .and_then(|value| value.as_string());
                let version_req = obj
                    .get_by_name("version")
                    .and_then(|value| value.as_string());

                if let (Some(address), Some(version_req)) = (address, version_req) {
                    let loc = hcl::compute_location(address, &entry)
                        .with_span(version_req.span().start(), version_req.span().end());

                    let source = VersionedSource {
                        source: TerraformSource {
                            kind: TerraformSourceKind::Provider,
                            address: address.to_string(),
                        }
                        .into(),
                        current: Some(version_req.to_string()),
                        required: Some(version_req.to_string()),
                    };

                    sources.push(Ok(source.with_location(loc)));
                }
            }
        }
    }

    // Parse modules and inline hints
    for module in ast.find_blocks("module") {
        // Parse modules
        let address = module
            .get_attribute("source")
            .and_then(|attr| attr.expr.as_string());
        let version_req = module
            .get_attribute("version")
            .and_then(|attr| attr.expr.as_string());

        if let Some(address) = address {
            // Apply address filter
            if address.starts_with("git::")
                || address.starts_with("./")
                || address.starts_with("../")
            {
                continue;
            }

            let loc = hcl::compute_location_with_span(address, version_req, &entry);

            let source = VersionedSource {
                source: TerraformSource {
                    kind: TerraformSourceKind::Module,
                    address: address.to_string(),
                }
                .into(),
                current: version_req.map(|value| value.to_string()),
                required: version_req.map(|value| value.to_string()),
            };

            sources.push(Ok(source.with_location(loc)));
        }

        // Parse inline hints

        let inline_hints: Vec<&Attribute> = module
            .get_attributes_with_inline_hints()
            .into_iter()
            .filter(|a| matches!(a.hint.as_ref().unwrap(), InlineHint::Source(_)))
            .collect();

        for attribute in inline_hints {
            let InlineHint::Source(source_hint) = attribute.hint.as_ref().unwrap();

            let repository = source_hint.address;
            let repo_string = hcl::convert_into_string(repository);

            let repo_as_url = match Url::parse(hcl::convert_into_string(repository).as_str()) {
                Ok(url) => url,
                Err(err) => {
                    sources.push(Err(Error::InvalidUrl {
                        repo_url: repo_string,
                        err,
                    }));
                    continue;
                }
            };

            // Apply address filter
            if repository.starts_with("git::")
                || repository.starts_with("./")
                || repository.starts_with("../")
            {
                continue;
            }

            let version_constraint = source_hint.version_constraint.map(hcl::convert_into_string);

            let version = attribute.clone().second_term_as_string();
            let loc = hcl::compute_location_with_span(repository, version, &entry);

            let source = VersionedSource {
                source: match BaseSource::parse_url(&repo_as_url) {
                    Ok(source) => source.into(),
                    Err(err) => {
                        sources.push(Err(Error::invalid_inline_hint(err, repository.to_string())));
                        continue;
                    }
                },
                current: Some(version.unwrap().to_string()),
                required: version_constraint,
            };

            sources.push(Ok(source.with_location(loc)));
        }
    }

    Ok(sources)
}

#[cfg(_test)]
mod tests {
    use itertools::Itertools;

    use super::parse_dir;
    use crate::sources::{
        parse::Options, Location, Source, SourceKind, TerraformSourceKind, VersionedSource,
    };

    const TEST_DIRECTORY: &str = "tests/sources/parse";

    fn result_non_recursive() -> Vec<VersionedSource> {
        vec![
            VersionedSource {
                source: Source {
                    loc: Location::new("main.sample.test.tf", 4, 17).with_span(100, 108),
                    kind: SourceKind::Terraform {
                        kind: TerraformSourceKind::Provider,
                        address: "gitlabhq/gitlab".to_string(),
                    },
                },
                version_constraint: Some("~> 3.8".to_string()),
                version: Some("~> 3.8".to_string()),
            },
            VersionedSource {
                source: Source {
                    loc: Location::new("main.sample.test.tf", 12, 18).with_span(271, 278),
                    kind: SourceKind::Terraform {
                        kind: TerraformSourceKind::Module,
                        address: "gitlab.com/vigigloo/tf-modules/k8slimitednamespace".to_string(),
                    },
                },
                version_constraint: Some("0.3.5".to_string()),
                version: Some("0.3.5".to_string()),
            },
            VersionedSource {
                source: Source {
                    loc: Location::new("main.sample.test.tf", 17, 18),
                    kind: SourceKind::Terraform {
                        kind: TerraformSourceKind::Module,
                        address: "gitlab.com/vigigloo/tf-modules/k8slimitednamespace".to_string(),
                    },
                },
                version_constraint: None,
                version: None,
            },
            VersionedSource {
                source: Source {
                    loc: Location::new("main.sample.test.tf", 21, 13).with_span(477, 484),
                    kind: SourceKind::Terraform {
                        kind: TerraformSourceKind::Module,
                        address: "gitlab.com/vigigloo/tools-k8s/mattermost".to_string(),
                    },
                },
                version_constraint: Some("0.1.0".to_string()),
                version: Some("0.1.0".to_string()),
            },
            VersionedSource {
                source: Source {
                    loc: Location::new("main.sample.test.tf", 29, 22).with_span(780, 788),
                    kind: SourceKind::InlineHints {
                        repository: Box::new(SourceKind::HelmChart {
                            repository: "https://helm.nginx.com/stable".to_string(),
                            name: "nginx-ingress".to_string(),
                        }),
                    },
                },
                version_constraint: Some("> 0.10.0 , < 0.18.0".to_string()),
                version: Some("0.17.1".to_string()),
            },
            VersionedSource {
                source: Source {
                    loc: Location::new("main.sample.test.tf", 33, 22).with_span(963, 970),
                    kind: SourceKind::InlineHints {
                        repository: Box::new(SourceKind::Terraform {
                            kind: TerraformSourceKind::Module,
                            address: "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
                                .to_string(),
                        }),
                    },
                },
                version_constraint: Some("2.0.0".to_string()),
                version: Some("2.0.0".to_string()),
            },
        ]
    }

    fn result_non_recursive_with_hidden() -> Vec<VersionedSource> {
        let mut vec = result_non_recursive();
        vec.extend(vec![VersionedSource {
            source: Source {
                loc: Location::new(".hidden.sample.test.tf", 4, 17).with_span(100, 107),
                kind: SourceKind::Terraform {
                    kind: TerraformSourceKind::Provider,
                    address: "gitlabhq/gitlab".to_string(),
                },
            },
            version_constraint: Some("> 3.0".to_string()),
            version: Some("> 3.0".to_string()),
        }]);
        vec
    }

    fn result_recursive() -> Vec<VersionedSource> {
        let mut vec = result_non_recursive();
        vec.extend(vec![VersionedSource {
            source: Source {
                loc: Location::new("subdir/child.sample.test.tf", 2, 18).with_span(118, 125),
                kind: SourceKind::Terraform {
                    kind: TerraformSourceKind::Module,
                    address: "gitlab.com/vigigloo/tf-modules/k8slimitednamespace".to_string(),
                },
            },
            version_constraint: Some("0.1.0".to_string()),
            version: Some("0.1.0".to_string()),
        }]);
        vec
    }

    #[tokio::test]
    async fn parse_succeeds() {
        use std::default::Default;

        let (versions, errors): (Vec<_>, Vec<_>) = (parse_dir(&Default::default(), TEST_DIRECTORY))
            .await
            .into_iter()
            .partition_result();
        assert_eq!(versions, result_non_recursive());
        assert_eq!(errors.len(), 1);
    }

    #[tokio::test]
    async fn parse_succeeds_with_hidden() {
        let (mut versions, errors): (Vec<_>, Vec<_>) = (parse_dir(
            &Options {
                recursive: false,
                hidden: true,
            },
            TEST_DIRECTORY,
        ))
        .await
        .into_iter()
        .partition_result();

        let mut wanted = result_non_recursive_with_hidden();
        wanted.sort_unstable();
        versions.sort_unstable();

        assert_eq!(versions, wanted);
        assert_eq!(errors.len(), 1);
    }

    #[tokio::test]
    async fn parse_succeeds_recursive() {
        let (mut versions, errors): (Vec<_>, Vec<_>) = (parse_dir(
            &Options {
                recursive: true,
                hidden: false,
            },
            TEST_DIRECTORY,
        ))
        .await
        .into_iter()
        .partition_result();

        let mut wanted = result_recursive();
        wanted.sort_unstable();
        versions.sort_unstable();

        assert_eq!(versions, wanted);
        assert_eq!(errors.len(), 1);
    }
}
