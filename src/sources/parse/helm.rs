use std::path::Path;

use super::ParseOptions;
use crate::sources::VersionedSource;
use crate::{Error, Result};

pub async fn parse_dir(
    path: impl AsRef<Path>,
    opts: &ParseOptions,
) -> impl IntoIterator<Item = Result<VersionedSource>> {
    use futures::future::join_all;
    use itertools::Itertools;

    let pattern = if opts.recursive {
        "**/Chart.yaml"
    } else {
        "Chart.yaml"
    };

    let glob = wax::Glob::new(pattern).unwrap();
    let walker = glob
        .walk(path)
        .filter_map(|entry| entry.ok())
        .filter(|entry| opts.hidden || !entry.to_candidate_path().as_ref().starts_with('.'))
        .map(parse_chart);

    join_all(walker).await.into_iter().flatten_ok()
}

async fn parse_chart(walk_entry: wax::WalkEntry<'_>) -> Result<Vec<VersionedSource>> {
    let mut path = walk_entry.to_candidate_path().to_string();
    if path.is_empty() {
        path = "Chart.yaml".to_string();
    }

    let chart_content = tokio::fs::read_to_string(walk_entry.path())
        .await
        .map_err(|err| Error::io(err, "Failed to read helm chart"))?;
    let root_node = marked_yaml::parse_yaml(0, &chart_content)?;
    let root = root_node
        .as_mapping()
        .expect("Chart.yaml should be a mapping");

    let Some(deps) = root.get_sequence("dependencies") else {
        return Ok(vec![]);
    };

    Ok(deps
        .iter()
        .filter_map(|entry| {
            use super::Location;
            use crate::sources::data::HelmSource;
            use crate::util::parse;

            let entry = entry.as_mapping()?;

            let version_node = entry.get_scalar("version")?;
            let version_start = version_node.span().start()?;
            let version_start_idx = parse::loc_to_index(
                &chart_content,
                version_start.line() as u32,
                version_start.column() as u32,
            )?;
            let mut version_end_idx = version_start_idx + version_node.len();
            // Fix case when string is quoted and marked-yaml doesn't correctly handle this.
            if chart_content.as_bytes()[version_start_idx] == b'"' {
                version_end_idx += 2;
            }

            let name = entry.get_scalar("name")?.as_str();
            let version = version_node.as_str();
            let repository = entry.get_scalar("repository").map(|v| v.as_str())?;

            let loc = Location::new(
                path.clone(),
                version_start.line() as u32,
                version_start.column() as u32,
            )
            .with_span(version_start_idx, version_end_idx);

            let source = HelmSource {
                repository: repository.to_string(),
                name: name.to_string(),
            };

            let versioned_source = VersionedSource {
                source: source.into(),
                current: Some(version.to_owned()),
                required: Some(version.to_owned()),
            };

            Some(versioned_source.with_location(loc))
        })
        .collect())
}

#[cfg(_test)]
mod tests {
    use itertools::Itertools;

    use super::parse_dir;
    use crate::sources::{data::HelmSource, parse::ParseOptions, VersionedSource};

    const TEST_DIRECTORY: &str = "tests/sources/parse";

    #[tokio::test]
    async fn parse_chart() {
        let (mut versions, errors): (Vec<_>, Vec<_>) = (parse_dir(
            TEST_DIRECTORY,
            &ParseOptions {
                recursive: false,
                hidden: true,
            },
        ))
        .await
        .into_iter()
        .partition_result();

        let expected = vec![VersionedSource {
            source: Source {
                loc: Location::new("Chart.yaml", 6, 14).with_span(92, 100),
                kind: SourceKind::HelmChart {
                    name: "nginx-ingress".to_string(),
                    repository: "https://helm.nginx.com/stable".to_string(),
                },
            },
            version_constraint: Some("0.17.0".to_string()),
            version: Some("0.17.0".to_string()),
        }];

        versions.sort_unstable();

        assert_eq!(versions, expected);
        assert!(errors.is_empty());
    }
}
