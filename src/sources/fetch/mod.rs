use std::path::Path;

use smart_default::SmartDefault;

use super::Source;
use crate::Result;

#[derive(Debug, Clone, SmartDefault)]
pub struct FetchOptions {}

pub async fn fetch_source(_source: &Source, _dest: &Path, _options: &FetchOptions) -> Result<()> {
    todo!()
}
