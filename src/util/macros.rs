macro_rules! enum_from {
    (
        $ty:ty {
            $($from_ty:ty => $variant:ident),+$(,)?
        }
    ) => {
        $(
            impl From<$from_ty> for $ty {
                fn from(value: $from_ty) -> $ty {
                    Self::$variant(value)
                }
            }
        )+
    };
}
