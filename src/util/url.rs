#[easy_ext::ext(UrlExt)]
pub impl url::Url {
    fn scheme_pair(&self) -> (&str, Option<&str>) {
        let scheme = self.scheme();

        if let Some((main, secondary)) = scheme.split_once('+') {
            (main, Some(secondary))
        } else {
            (scheme, None)
        }
    }

    fn scheme_main(&self) -> &str {
        self.scheme_pair().0
    }

    fn scheme_secondary(&self) -> Option<&str> {
        self.scheme_pair().1
    }
}
