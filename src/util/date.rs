use chrono::{DateTime, Datelike, Duration as chronoDuration, Local, OutOfRangeError, Timelike};
use std::time::Duration;

pub fn date_from_duration(duration: Duration) -> Result<DateTime<Local>, OutOfRangeError> {
    let today = Local::now();
    Ok(today - chronoDuration::from_std(duration)?)
}

pub fn duration_from_date(date: DateTime<Local>) -> Result<Duration, OutOfRangeError> {
    let today = Local::now();
    (today - date).to_std()
}

pub fn duration_to_formatted_string(duration: Duration) -> String {
    use regex::Regex;

    let mut duration_str = humantime::Duration::from(duration)
        .to_string()
        .replace("day", " day")
        .replace("week", " week")
        .replace("month", " month")
        .replace("year", " year");

    // remove millisecond and microsecond
    let regex = Regex::new(r"([0-9]{2,3}?ms)|([0-9]{2,3}?us)").unwrap();
    duration_str = regex.replace_all(duration_str.as_str(), "").to_string();

    duration_str
}

pub fn date_to_formatted_string(date: DateTime<Local>) -> String {
    format!(
        "{:02}/{:02}/{:02} at {:02}:{:02}:{:02}",
        date.day(),
        date.month(),
        date.year(),
        date.hour(),
        date.minute(),
        date.second()
    )
}
