macro_rules! tag {
    ($tag:expr) => {
        $crate::sources::list::Tag::new_without_date($tag)
    };
}

macro_rules! tags {
    () => {
        Vec::new()
    };
    ($($tag:expr),+$(,)?) => {
        vec![$(tag!($tag)),+]
    };
}

// Extracted from `matches` crate
macro_rules! assert_matches {
    ($expression:expr, $($pattern:tt)+) => {
        match $expression {
            $($pattern)+ => (),
            ref e => panic!("assertion failed: `{:?}` does not match `{}`", e, stringify!($($pattern)+)),
        }
    }
}
