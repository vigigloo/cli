pub fn loc_to_index(s: &str, line: u32, column: u32) -> Option<usize> {
    let mut current = (1, 1);

    for (idx, c) in s.char_indices() {
        if current == (line, column) {
            return Some(idx);
        }

        if c == '\n' {
            current.0 += 1;
            current.1 = 1;
        } else {
            current.1 += 1;
        }
    }

    None
}
