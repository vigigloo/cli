#![allow(dead_code)]
use std::borrow::Cow;
use std::path::Path;
use std::{io, ops};

use ropey::Rope;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Action<'a> {
    Delete(Option<usize>, Option<usize>),
    Insert(usize, Cow<'a, str>),
    Replace(Option<usize>, Option<usize>, Cow<'a, str>),
}

impl<'a> Action<'a> {
    pub fn delete<R: ops::RangeBounds<usize>>(range: R) -> Self {
        let start = index_bound_to_option(range.start_bound().cloned());
        let end = index_bound_to_option(range.end_bound().cloned());

        Self::Delete(start, end)
    }

    pub fn insert(idx: usize, text: impl Into<Cow<'a, str>>) -> Self {
        Self::Insert(idx, text.into())
    }

    pub fn replace<R: ops::RangeBounds<usize>>(range: R, text: impl Into<Cow<'a, str>>) -> Self {
        let start = index_bound_to_option(range.start_bound().cloned());
        let end = index_bound_to_option(range.end_bound().cloned());

        Self::Replace(start, end, text.into())
    }
}

fn index_bound_to_option(bound: ops::Bound<usize>) -> Option<usize> {
    match bound {
        ops::Bound::Included(idx) => Some(idx),
        ops::Bound::Excluded(idx) => Some(idx - 1),
        ops::Bound::Unbounded => None,
    }
}

pub fn edit_file(path: &Path, actions: &[Action]) -> io::Result<()> {
    use std::fs::File;
    use std::io::{BufReader, BufWriter};

    let mut text = Rope::from_reader(BufReader::new(File::open(path)?))?;
    edit_rope(&mut text, actions);
    text.write_to(BufWriter::new(File::create(path)?))?;

    Ok(())
}

pub fn edit(input: impl Into<Rope>, actions: &[Action]) -> String {
    let mut input = input.into();
    edit_rope(&mut input, actions);
    input.into()
}

fn edit_rope(input: &mut Rope, actions: &[Action]) {
    use itertools::Itertools;

    let actions = actions
        .iter()
        .sorted_by_key(|action| match action {
            Action::Delete(start, _) => *start,
            Action::Insert(idx, _) => Some(*idx),
            Action::Replace(start, _, _) => *start,
        })
        .rev();
    for action in actions {
        apply_action(input, action);
    }
}

#[inline]
fn apply_action(input: &mut Rope, action: &Action) {
    match action {
        Action::Delete(start, end) => match (start, end) {
            (Some(start), Some(end)) => input.remove(start..end),
            (Some(start), None) => input.remove(start..),
            (None, Some(end)) => input.remove(..end),
            (None, None) => input.remove(..),
        },
        Action::Insert(idx, text) => input.insert(*idx, text),
        Action::Replace(start, end, text) => {
            apply_action(input, &Action::Delete(*start, *end));
            input.insert(start.unwrap_or(0), text);
        }
    }
}
