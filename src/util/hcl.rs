use crate::parsers::hcl::ast::Str;
use crate::sources::parse::Location;
use wax::WalkEntry;

/// Convert `str` into a String and remove extra `"` around the String
pub fn convert_into_string(str: Str) -> String {
    let binding = str.to_string();
    let binding_as_str = binding.as_str();
    binding_as_str[1..binding_as_str.len() - 1].to_string()
}

pub fn compute_location(repository: Str, entry: &WalkEntry) -> Location {
    let (line, col) = repository.span().start_pos().line_col();

    Location::new(
        entry.to_candidate_path().to_string(),
        line as u32,
        col as u32,
    )
}

pub fn compute_location_with_span(
    repository: Str,
    version_req: Option<Str>,
    entry: &WalkEntry,
) -> Location {
    let loc = compute_location(repository, entry);

    match version_req.map(|v| v.span()) {
        Some(span) => loc.with_span(span.start(), span.end()),
        None => loc,
    }
}
