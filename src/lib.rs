use semver::Version;

pub use config::Config;
pub use error::{Error, Result, Results};

#[macro_use]
mod util;

pub mod ops;
pub mod parsers;
pub mod sources;

pub mod config;
pub mod error;

pub mod build_info {
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

fn version() -> &'static Version {
    use once_cell::sync::OnceCell;

    static INSTANCE: OnceCell<Version> = OnceCell::new();
    INSTANCE.get_or_init(|| Version::parse(env!("CARGO_PKG_VERSION")).unwrap())
}
