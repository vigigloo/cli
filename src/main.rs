use std::path::PathBuf;

use clap::Parser;
use miette::Result;

mod commands;

const LONG_VERSION: &str = const_format::formatcp!(
    "\
{version} ({build_version} {profile} {build_target})

Build time   : {build_time}
Git commit   : {git_commit}
Rust version : {rust_version}",
    version = vigigloo::build_info::PKG_VERSION,
    profile = vigigloo::build_info::PROFILE,
    build_version = if let Some(version) = vigigloo::build_info::GIT_VERSION {
        version
    } else {
        const_format::formatcp!("v{}", vigigloo::build_info::PKG_VERSION)
    },
    build_time = vigigloo::build_info::BUILT_TIME_UTC,
    build_target = vigigloo::build_info::TARGET,
    git_commit = if let Some(commit_id) = vigigloo::build_info::GIT_COMMIT_HASH {
        commit_id
    } else {
        "N/A"
    },
    rust_version = vigigloo::build_info::RUSTC_VERSION,
);

#[derive(Debug, Parser)]
#[command(version, author, about, long_version = LONG_VERSION)]
struct Options {
    #[arg(
        short,
        long,
        env = "VIGIGLOO_CONFIG_FILE",
        default_value = "vigigloo.yml"
    )]
    config: PathBuf,

    #[command(subcommand)]
    subcommand: commands::Command,
}

#[tokio::main]
async fn main() -> Result<()> {
    setup_logger();

    let options = Options::parse();
    let config = if options.config.exists() {
        Some(vigigloo::Config::load(&options.config)?)
    } else {
        None
    };

    options.subcommand.run(config.as_ref()).await
}

fn setup_logger() {
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{filter::LevelFilter, fmt, EnvFilter};

    let fmt_layer = fmt::layer().without_time().with_target(false);

    let filter_layer = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .from_env_lossy();

    tracing_subscriber::registry()
        .with(filter_layer)
        .with(fmt_layer)
        .init();
}
