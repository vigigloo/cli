use std::path::Path;

use semver::VersionReq;
use serde::Deserialize;

use crate::ops::check;
use crate::ops::run::codegen as workflow;
use crate::{Error, Result};

#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
pub struct Config {
    #[serde_as(as = "serde_with::DisplayFromStr")]
    pub version: VersionReq,
    #[serde(default)]
    pub workflow: Vec<workflow::Step>,
    #[serde(default)]
    pub check: check::Config,
}

impl Config {
    pub fn load(path: &Path) -> Result<Self> {
        use std::fs::File;

        let mut path = path.to_path_buf();

        if path.is_dir() {
            path = path.join("vigigloo.yml");
        }

        let file = File::open(&path).map_err(|err| {
            Error::io(
                err,
                format!("Could not open config file: {}", path.display()),
            )
        })?;

        let config: Config =
            serde_yaml::from_reader(file).map_err(|err| Error::deserialize_config(err, path))?;

        config.check_version()?;

        Ok(config)
    }

    pub(crate) fn check_version(&self) -> Result<()> {
        if !self.version.matches(crate::version()) {
            return Err(Error::UnsupportedConfigVersion {
                required: self.version.to_string(),
                current: crate::version().to_string(),
            });
        }

        Ok(())
    }
}
