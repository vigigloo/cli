use crate::sources::{list, parse, resolve};
use crate::Result;

pub mod apply;
pub mod display;

#[derive(Default)]
pub struct Options<'a> {
    pub parse: parse::ParseOptions,
    pub list: list::ListOptions<'a>,
    pub resolve: resolve::ResolveOptions,
}

pub async fn run(options: Options<'_>) -> Result<()> {
    use itertools::Itertools;

    let (sources, errors) = parse::parse_cwd(&options.parse).await.split();
    parse::print_parsing_errors(errors);

    let sources = sources.iter().map(|source| async {
        let tags = list::list_tags(&source.source, &options.list)
            .await
            .map_err(|err| err.with_data(source.clone()))?;
        resolve::resolve_tags(source, &tags, &options.resolve)
    });
    let results: Vec<_> = futures::future::join_all(sources)
        .await
        .into_iter()
        .filter_map_ok(|source| source)
        .collect();

    let update_sources: Vec<_> = results.iter().filter_map(|res| res.as_ref().ok()).collect();
    apply::apply(update_sources)?;

    display::display_results(results.into());

    Ok(())
}
