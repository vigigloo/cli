use comfy_table::Cell;

use crate::sources::{print_errors, resolve::ResolvedSource, VersionedSource};
use crate::{error::ErrorWithData, Results};

pub fn display_results(results: Results<ResolvedSource, ErrorWithData<VersionedSource>>) {
    use itertools::Itertools;

    let (update_sources, errors): (Vec<_>, Vec<_>) = results.split();

    let update_sources_groups = update_sources
        .into_iter()
        .into_group_map_by(|source| source.source.source.to_string())
        .into_iter()
        .sorted_unstable_by_key(|(key, _)| key.clone());

    for (kind, sources) in update_sources_groups {
        print_table(kind, sources);
    }

    print_errors(errors);
}

fn print_table(kind: String, sources: Vec<ResolvedSource>) {
    use comfy_table::{presets::UTF8_FULL, ColumnConstraint::*, ContentArrangement, Table};

    let mut table = Table::new();
    let is_inline_hint = kind == "Inline Hints";

    let header = if is_inline_hint {
        vec![
            kind.as_str(),
            "Current",
            "Constraint",
            "Wanted",
            "Latest",
            "Location",
            "Updated",
        ]
    } else {
        vec![
            kind.as_str(),
            "Current",
            "Wanted",
            "Latest",
            "Location",
            "Updated",
        ]
    };

    table
        .load_preset(UTF8_FULL)
        .set_content_arrangement(ContentArrangement::DynamicFullWidth)
        .set_width(100)
        .set_header(header)
        .column_mut(0)
        .expect("this should be first column")
        .set_constraint(UpperBoundary(comfy_table::Width::Fixed(30)));

    let mut ordered_sources = sources;
    ordered_sources.sort_by(|a, b| b.wanted.is_some().cmp(&a.wanted.is_some()));

    ordered_sources.iter().for_each(|s| {
        table.add_row(update_source_to_cell_vec(s, is_inline_hint));
    });

    println!("{table}");
}

fn update_source_to_cell_vec(source: &ResolvedSource, with_constraint: bool) -> Vec<Cell> {
    use comfy_table::Color;

    let current = source.source.current.as_deref().unwrap_or("N/A");
    let wanted;
    let updated_cell;

    match source.wanted {
        None => {
            wanted = "N/A".to_string();
            updated_cell = Cell::new("no").fg(Color::DarkYellow);
        }
        Some(ref version) => {
            wanted = version.clone();
            updated_cell = Cell::new("yes").fg(Color::DarkGreen);
        }
    }

    if with_constraint {
        let constraint = source.source.required.as_deref().unwrap_or("N/A");
        vec![
            Cell::new(source.source.source.describe()),
            Cell::new(current.to_string()),
            Cell::new(constraint),
            Cell::new(&wanted),
            Cell::new(source.latest.to_string()),
            Cell::new(source.source.source.location().unwrap().to_string()),
            updated_cell,
        ]
    } else {
        vec![
            Cell::new(source.source.source.describe()),
            Cell::new(current.to_string()),
            Cell::new(&wanted),
            Cell::new(source.latest.to_string()),
            Cell::new(source.source.source.location().unwrap().to_string()),
            updated_cell,
        ]
    }
}
