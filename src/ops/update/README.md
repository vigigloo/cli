# Update command

Updates outdated modules/providers from terraform files and outdated dependencies from helm charts to their _latest_ version (not the _wanted_ one from `outdated` display) . Versions that use version matching aren't updated.

## Usage

```shell
$ vigigloo update
```
