use crate::sources::resolve::ResolvedSource;
use crate::{Error, Result};

pub fn apply<'a>(sources: impl IntoIterator<Item = &'a ResolvedSource>) -> Result<()> {
    use itertools::Itertools;

    use crate::util::edit;

    for (path, sources) in &sources
        .into_iter()
        .sorted_by_key(|source| source.source.source.location().unwrap().path())
        .group_by(|source| source.source.source.location().unwrap().path())
    {
        let actions: Vec<_> = sources
            .filter(|source| source.wanted.is_some())
            .filter_map(|source| {
                Some((
                    source.source.source.location().unwrap().span()?,
                    &source.wanted,
                ))
            })
            .map(|((start, end), version)| {
                edit::Action::replace(start..=end, format!("\"{}\"", version.as_ref().unwrap()))
            })
            .collect();
        edit::edit_file(path, &actions)
            .map_err(|err| Error::io(err, format!("Could not edit file: {}", path.display())))?;
    }

    Ok(())
}
