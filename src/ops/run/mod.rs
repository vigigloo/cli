use crate::Config;
use crate::Result;

pub mod codegen;

pub async fn run(config: &Config) -> Result<()> {
    tracing::debug!("Config: {config:#?}");

    for entry in &config.workflow {
        entry.execute().await?;
    }

    Ok(())
}
