# Run command

Tasks executor using the workflow entry in config file as a playbook

## Usage

```shell
$ vigigloo run
```

## Config file

Must contain a `workflow` entry with a list of steps. Each step is named and contains a list of actions.

<i>Example</i>

```yaml
workflow:
  - name: documentation generation
    actions:
      - import:
          source: !gitlab vigigloo/gitlab-pipeline-fragments
      - document:
          show_warnings: true
          path: documentation
  - name: Fetch Scaphandre Helm Chart
    actions:
      - import:
          source: !github hubblo-org/scaphandre
          directory: helm/scaphandre
      - output:
          path: helm_chart
```

## Actions

Actions can be considered as parts of a pipe, some must be at the beginning, others must be at the end.

### Beginning

###### import

Import a folder from a source (git, archive, etc)

Must contain a `source`: URL to archive, repo git/gitlab/github.\
Can contain a `directory` : directory within `source`.

###### local

Import a folder from a local path

Must contain a `path`: absolute or relative local path.

### Middle

###### copy

Copy contents from `path` to either `dest`(if some) or pipe for next action.

Must contain a `path`: absolute or relative local path within input pipe content.\
Can contain a `dest`: absolute or relative local path within output pipe content.

###### template

Transform actions pipe input with a Template engine.

Must contain a `engine`: By now, only `Tera`is available.\
Can contain a `glob`: glob to getting templates.\
Must contain a `variables`: hashmap key/value, variables in template.

### End

###### output

Copy folders from previous actions from pipe.

Must contain a `path`: absolute or relative local path.\
Can contain a `clean`: (default `true`) if true, output directory will be dropped before the copy.

###### document

Creates documentation folder, by copying existing .md file and creating new ones from .yml files.
Comments from .yml files will be copied as documentation if they begin with `##`instead of `#`.

Must contain a `path`: absolute or relative local path.\
Can contain a `show_warnings`: (default `true`) if true, a warning will be display for each folder created without providing README.md file.\
Can contain a `clean`: (default `true`) if true, output directory will be dropped before the copy.
