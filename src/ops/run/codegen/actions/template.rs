use std::collections::HashMap;
use std::path::Path;

use serde::Deserialize;
use wax::WalkEntry;

use super::Pipe;
use super::{Error, Result};

/// Transform actions pipe input with a Template engine
#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
pub struct Action {
    #[serde(default)]
    engine: Engine,
    #[serde_as(as = "Option<serde_with::DisplayFromStr>")]
    glob: Option<String>,
    variables: HashMap<String, String>,
}

impl Action {
    pub async fn execute(&self, pipe: &mut Pipe) -> Result<()> {
        use dircpy::CopyBuilder;

        let input = pipe.input().ok_or(Error::NoInputInPipe)?;
        let output = pipe.output_temp()?;

        tracing::info!(
            "Rendering template: {} -> {}",
            input.display(),
            output.display(),
        );

        CopyBuilder::new(&input, &output)
            .overwrite(true)
            .run()
            .map_err(|err| Error::CouldNotCopyDirectory {
                source: input.clone(),
                destination: output.clone(),
                err,
            })?;

        let pattern = self.glob.as_deref().unwrap_or("**/*");
        let glob = wax::Glob::new(pattern)?;

        let walker = glob.walk(&input).filter_map(|entry| entry.ok());

        match self.engine {
            Engine::Tera => self.generate_tera(&input, &output, pattern, walker).await,
        }
    }

    async fn generate_tera(
        &self,
        input: &Path,
        output: &Path,
        pattern: &str,
        walker: impl Iterator<Item = WalkEntry<'static>>,
    ) -> Result<()> {
        use tera::{Context, Tera};

        let templates_dir = format!("{input}/{pattern}", input = input.display());
        let tera = Tera::new(&templates_dir)?;

        let mut context = Context::new();
        for (key, value) in self.variables.iter() {
            let value = shellexpand::env(value)?;
            context.try_insert(key, &value)?;
        }

        for entry in walker {
            use std::fs::File;

            // Safe to unwrap here, we're already supposed to be in `input` directory
            let rel_path = entry.path().strip_prefix(input).unwrap();
            let output_path = output.join(rel_path);

            let output_file =
                File::create(&output_path).map_err(|err| Error::CouldNotOpenFile {
                    path: output_path,
                    err,
                })?;
            tera.render_to(&rel_path.to_string_lossy(), &context, output_file)?;
        }

        Ok(())
    }
}

#[derive(Debug, Default, Clone, Copy, Deserialize)]
#[serde(rename_all = "snake_case")]
enum Engine {
    #[default]
    #[serde(alias = "jinja2")]
    Tera,
}
