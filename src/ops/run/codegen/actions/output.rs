use std::path::PathBuf;

use super::Pipe;
use super::{Error, Result};
use serde::Deserialize;

/// Output a folder from actions pipe
#[derive(Debug, Deserialize)]
pub struct Action {
    path: PathBuf,
    /// if true or none, output directory will be dropped before the copy
    clean: Option<bool>,
}

impl Action {
    pub async fn execute(&self, pipe: &mut Pipe) -> Result<()> {
        use dircpy::CopyBuilder;

        let input = pipe.input().ok_or(Error::NoInputInPipe)?;

        if self.path.exists() && self.clean.unwrap_or(true) {
            tokio::fs::remove_dir_all(&self.path).await.map_err(|e| {
                Error::CouldNotDeleteDirectory {
                    path: self.path.clone(),
                    err: e,
                }
            })?;
        }
        CopyBuilder::new(&input, &self.path)
            .overwrite(true)
            .run()
            .map_err(|e| Error::CouldNotCopyDirectory {
                source: input.to_path_buf(),
                destination: self.path.clone(),
                err: e,
            })
    }
}
