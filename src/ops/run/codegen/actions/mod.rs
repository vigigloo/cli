use std::ops::Deref;
use std::path::{Path, PathBuf};

use serde::Deserialize;
use tempdir::TempDir;

use crate::{Error, Result};

mod copy;
mod document;
mod import;
mod local;
mod output;
mod template;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Action {
    Copy(copy::Action),
    Import(import::Action),
    Local(local::Action),
    Output(output::Action),
    Template(template::Action),
    Document(document::Action),
}

impl Action {
    pub async fn execute(&self, pipe: &mut Pipe) -> Result<()> {
        match self {
            Action::Copy(ref action) => action.execute(pipe).await,
            Action::Import(ref action) => action.execute(pipe).await,
            Action::Local(ref action) => action.execute(pipe).await,
            Action::Output(ref action) => action.execute(pipe).await,
            Action::Template(ref action) => action.execute(pipe).await,
            Action::Document(ref action) => action.execute(pipe).await,
        }
    }
}

#[derive(Debug)]
pub struct Pipe(Option<PipePath>, Option<PipePath>);

impl Pipe {
    #[inline]
    pub const fn new() -> Pipe {
        Pipe(None, None)
    }

    #[inline]
    pub fn pipe(self) -> Pipe {
        Pipe(self.1, None)
    }

    #[inline]
    pub fn input(&self) -> Option<PathBuf> {
        self.0.as_deref().map(|path| path.to_path_buf())
    }

    #[inline]
    pub fn output(&mut self, path: impl Into<PipePath>) -> PathBuf {
        self.1.insert(path.into()).to_path_buf()
    }

    #[inline]
    pub fn output_path(&mut self, path: &Path) -> PathBuf {
        self.output(path)
    }

    #[inline]
    pub fn output_temp(&mut self) -> Result<PathBuf> {
        Ok(self.output(PipePath::temp()?))
    }

    #[allow(dead_code)]
    #[inline]
    pub fn output_temp_with_prefix(&mut self, prefix: &str) -> Result<PathBuf> {
        Ok(self.output(PipePath::temp_with_prefix(prefix)?))
    }
}

#[derive(Debug)]
pub enum PipePath {
    Path(PathBuf),
    Temp(TempDir),
}

impl PipePath {
    fn path(path: &Path) -> PipePath {
        PipePath::Path(path.into())
    }

    fn temp() -> Result<PipePath> {
        Self::temp_with_prefix("vigigloo")
    }

    fn temp_with_prefix(prefix: &str) -> Result<PipePath> {
        Ok(PipePath::Temp(
            TempDir::new(prefix).map_err(Error::TempDirectory)?,
        ))
    }

    pub fn as_path(&self) -> &Path {
        match self {
            PipePath::Path(ref path) => path,
            PipePath::Temp(ref dir) => dir.path(),
        }
    }
}

impl<'a> From<&'a Path> for PipePath {
    fn from(path: &'a Path) -> Self {
        Self::path(path)
    }
}

impl From<PathBuf> for PipePath {
    fn from(path: PathBuf) -> Self {
        Self::Path(path)
    }
}

impl From<TempDir> for PipePath {
    fn from(tempdir: TempDir) -> Self {
        Self::Temp(tempdir)
    }
}

impl Deref for PipePath {
    type Target = Path;

    fn deref(&self) -> &Self::Target {
        self.as_path()
    }
}
