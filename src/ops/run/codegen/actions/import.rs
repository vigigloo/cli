use std::path::PathBuf;

use serde::Deserialize;

use super::{Error, Pipe, Result};
use crate::ops::run::codegen::source::Source;

/// Import a folder from a source (git, archive, etc.)
#[derive(Debug, Deserialize)]
pub struct Action {
    source: Source,
    directory: Option<PathBuf>,
}

impl Action {
    pub async fn execute(&self, pipe: &mut Pipe) -> Result<()> {
        let source = self.source.run().await?;

        match self.directory {
            Some(ref path) => {
                use dircpy::CopyBuilder;

                let output = pipe.output_temp()?;
                let source = source.join(path);

                CopyBuilder::new(&source, &output)
                    .overwrite(true)
                    .run()
                    .map_err(|err| Error::CouldNotCopyDirectory {
                        source,
                        destination: output.to_path_buf(),
                        err,
                    })?;
            }
            None => {
                pipe.output(source);
            }
        }

        Ok(())
    }
}
