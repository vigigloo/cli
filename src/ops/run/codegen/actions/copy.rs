use std::path::PathBuf;

use serde::Deserialize;

use super::{Error, Pipe, Result};

/// copy contents from `path` to either `dest`(if some) or pipe
#[derive(Debug, Deserialize)]
pub struct Action {
    path: PathBuf,
    dest: Option<PathBuf>,
}

impl Action {
    pub async fn execute(&self, pipe: &mut Pipe) -> Result<()> {
        use dircpy::CopyBuilder;

        let input = pipe.input().ok_or(Error::NoInputInPipe)?;
        let output = pipe.output_temp()?;

        let path = self.path.as_path();
        let dest = self.dest.as_deref().unwrap_or(path);

        tracing::info!("Copy `{}` to `{}`", self.path.display(), dest.display());

        CopyBuilder::new(&input, &output)
            .overwrite(true)
            .run()
            .map_err(|err| Error::CouldNotCopyDirectory {
                source: input.clone(),
                destination: output.clone(),
                err,
            })?;

        let dest = output.join(dest);

        if path.is_dir() {
            CopyBuilder::new(path, &dest)
                .overwrite(true)
                .run()
                .map_err(|err| Error::CouldNotCopyDirectory {
                    source: path.into(),
                    destination: dest,
                    err,
                })?;
        } else {
            tokio::fs::copy(path, &dest).await.map_err(|err| {
                Error::io(
                    err,
                    format!(
                        "Could not copy `{}` into `{}`",
                        path.display(),
                        dest.display()
                    ),
                )
            })?;
        }

        Ok(())
    }
}
