use std::path::PathBuf;

use serde::Deserialize;

use super::{Pipe, Result};

/// Import a folder from a local path
#[derive(Debug, Deserialize)]
pub struct Action {
    path: PathBuf,
}

impl Action {
    pub async fn execute(&self, pipe: &mut Pipe) -> Result<()> {
        pipe.output_path(&self.path);
        Ok(())
    }
}
