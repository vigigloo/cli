use serde::Deserialize;

use crate::util::serde::SingletonMap;
use crate::Result;

mod actions;
mod source;

#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
pub struct Step {
    name: String,
    #[serde_as(as = "Vec<SingletonMap>")]
    #[serde(default)]
    actions: Vec<actions::Action>,
}

impl Step {
    pub async fn execute(&self) -> Result<()> {
        use actions::Pipe;

        tracing::info!("Running step: {}", self.name);

        let mut pipe = Pipe::new();
        for action in self.actions.iter() {
            action.execute(&mut pipe).await?;

            // Update pipe for next action
            pipe = pipe.pipe();
        }

        Ok(())
    }
}
