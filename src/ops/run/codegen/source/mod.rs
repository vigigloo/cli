use std::fmt;
use std::ops::Deref;
use std::path::Path;
use std::str::FromStr;

use serde::Deserialize;
use tempdir::TempDir;

use super::actions::PipePath;
use crate::util::serde::MapOrString;
use crate::{Error, Result};

mod archive;
mod git;
mod github;
mod gitlab;

#[serde_with::apply(
    _ => #[serde_as(as = "MapOrString")],
)]
#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Source {
    Archive(archive::Source),
    Git(git::Source),
    Github(github::Source),
    Gitlab(gitlab::Source),
}

impl Source {
    pub async fn run(&self) -> Result<SourcePath> {
        match self {
            Source::Archive(ref source) => source.run().await,
            Source::Git(ref source) => source.run().await,
            Source::Github(ref source) => source.run().await,
            Source::Gitlab(ref source) => source.run().await,
        }
    }
}

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum SourceFormatError {}

impl FromStr for Source {
    type Err = SourceFormatError;

    fn from_str(_s: &str) -> Result<Self, Self::Err> {
        // TODO: Guess source type from string/url
        todo!()
    }
}

pub struct SourcePath(TempDir);

impl SourcePath {
    fn new() -> Result<Self> {
        Ok(Self(
            TempDir::new("vigigloo").map_err(Error::TempDirectory)?,
        ))
    }

    pub fn as_path(&self) -> &Path {
        self.0.path()
    }
}

impl AsRef<Path> for SourcePath {
    fn as_ref(&self) -> &Path {
        self.as_path()
    }
}

impl Deref for SourcePath {
    type Target = Path;

    fn deref(&self) -> &Self::Target {
        self.as_path()
    }
}

impl From<SourcePath> for PipePath {
    fn from(path: SourcePath) -> PipePath {
        PipePath::Temp(path.0)
    }
}

impl fmt::Display for SourcePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.display().fmt(f)
    }
}
