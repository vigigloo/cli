use std::path::Path;
use std::str::FromStr;

use reqwest::Response;
use serde::Deserialize;

use super::SourcePath;
use super::{Error, Result};

#[derive(Debug, Deserialize)]
pub struct Source {
    pub url: url::Url,
}

impl FromStr for Source {
    type Err = url::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Source { url: s.parse()? })
    }
}

impl Source {
    pub async fn run(&self) -> Result<SourcePath> {
        let source = SourcePath::new()?;

        tracing::info!("Downloading/Extracting archive: {} -> {source}", self.url);

        let path = Path::new(self.url.path());
        let filename = path
            .file_name()
            .map(|s| s.to_string_lossy())
            .ok_or_else(|| Error::invalid_archive(self.url.clone(), "Unknown archive type"))?;

        let extractor = if filename.ends_with(".tar.gz") {
            extract_tar_gz
        } else {
            return Err(Error::invalid_archive(
                self.url.clone(),
                "Unknown archive type",
            ));
        };

        let res = self.fetch().await?;
        extractor(source.as_path(), res).await?;

        Ok(source)
    }

    async fn fetch(&self) -> Result<Response> {
        let client = reqwest::Client::builder().build().map_err(|err| {
            Error::url_fetch(err, self.url.clone(), "Could not build HTTP/S client")
        })?;
        let req = client.get(self.url.clone()).build().map_err(|err| {
            Error::url_fetch(err, self.url.clone(), "Could not build HTTP/S request")
        })?;

        client.execute(req).await.map_err(|err| {
            Error::url_fetch(err, self.url.clone(), "Could not execute HTTP/S request")
        })
    }
}

async fn extract_tar_gz(dest: &Path, res: Response) -> Result<()> {
    use async_compression::tokio::bufread::GzipDecoder;
    use futures::TryStreamExt;
    use tokio_tar::Archive;
    use tokio_util::io::StreamReader;

    let stream = res.bytes_stream().map_err(|err| {
        use std::io::{Error, ErrorKind};

        Error::new(ErrorKind::Other, err)
    });
    let reader = StreamReader::new(stream);
    let decoder = GzipDecoder::new(reader);
    let mut archive = Archive::new(decoder);
    archive
        .unpack(dest)
        .await
        .map_err(|err| Error::io(err, "Could not extract archive"))?;

    Ok(())
}
