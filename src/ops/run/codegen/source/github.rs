use std::str::FromStr;

use serde::Deserialize;

use super::SourcePath;
use super::{Error, Result};

#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
pub struct Source {
    #[serde(flatten)]
    #[serde_as(as = "serde_with::TryFromInto<RepoSource>")]
    pub repo: RepoSourceNamed,
    #[serde(alias = "ref")]
    pub reference: Option<String>,
    #[serde(default = "Source::default_clone")]
    pub clone: bool,
}

impl Source {
    #[inline]
    fn default_clone() -> bool {
        true
    }
}

impl Source {
    pub async fn run(&self) -> Result<SourcePath> {
        if self.clone {
            use super::git::Source as GitSource;

            let source = GitSource {
                url: self.repo.to_url(),
                reference: self.reference.clone(),
            };

            source.run().await
        } else {
            use dircpy::CopyBuilder;

            use super::archive::Source as ArchiveSource;

            let default_branch = self.fetch_default_branch().await?;

            let repo_url = self.repo.to_url();
            let reference = self
                .reference
                .as_deref()
                .or(default_branch.as_deref())
                .unwrap_or("main");
            let url = format!("{repo_url}/archive/refs/heads/{reference}.tar.gz")
                .parse()
                .expect("URL should already be valid");

            let source = ArchiveSource { url };
            let temp_source = source.run().await?;
            let temp_source = temp_source.join(format!(
                "{repo_name}-{reference}",
                repo_name = self.repo.repo
            ));

            let source = SourcePath::new()?;

            CopyBuilder::new(&temp_source, &source)
                .overwrite(true)
                .run()
                .map_err(|err| Error::copy_directory(err, temp_source, source.as_path()))?;

            Ok(source)
        }
    }

    async fn fetch_default_branch(&self) -> Result<Option<String>> {
        let (owner, repo) = self.repo.to_pair();
        let repo = octocrab::instance()
            .repos(owner, repo)
            .get()
            .await
            .map_err(|err| {
                Error::github(err, format!("Could not get repository: {owner}/{repo}"))
            })?;
        Ok(repo.default_branch)
    }
}

impl FromStr for Source {
    type Err = SourceError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use std::collections::BTreeMap;

        let repo = RepoSourceNamed::from_str(s)?;

        let url = repo.to_url();
        let query_map: BTreeMap<_, _> = url.query_pairs().collect();

        let reference = query_map.get("ref");

        Ok(Source {
            repo,
            reference: reference.map(|reference| reference.to_string()),
            clone: Self::default_clone(),
        })
    }
}

#[derive(Debug, serde_with::DeserializeFromStr)]
pub enum RepoSource {
    Name(RepoSourceNamed),
    Url(url::Url),
}

impl FromStr for RepoSource {
    type Err = SourceError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("https") {
            Ok(RepoSource::Url(s.parse()?))
        } else {
            Ok(RepoSource::Name(s.parse()?))
        }
    }
}

#[derive(Debug)]
pub struct RepoSourceNamed {
    owner: String,
    repo: String,
}

impl RepoSourceNamed {
    fn to_url(&self) -> url::Url {
        let Self {
            ref owner,
            ref repo,
        } = self;
        format!("https://github.com/{owner}/{repo}")
            .parse()
            .unwrap()
    }

    #[inline]
    fn to_pair(&self) -> (&str, &str) {
        (&self.owner, &self.repo)
    }
}

impl FromStr for RepoSourceNamed {
    type Err = SourceError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (owner, repo) = match s.split('/').collect::<Vec<_>>()[..] {
            [owner, repo] => (owner, repo),
            _ => {
                return Err(SourceError::BadRepoFormat(s.to_owned()));
            }
        };

        Ok(RepoSourceNamed {
            owner: owner.to_owned(),
            repo: repo.to_owned(),
        })
    }
}

impl TryFrom<RepoSource> for RepoSourceNamed {
    type Error = SourceError;

    fn try_from(value: RepoSource) -> Result<Self, Self::Error> {
        match value {
            RepoSource::Name(named) => Ok(named),
            RepoSource::Url(ref url) => {
                let mut segments = match url.path_segments() {
                    Some(segments) => segments,
                    None => return Err(SourceError::BadRepoUrl(url.clone())),
                };

                let owner = match segments.next() {
                    Some(segment) => segment,
                    None => return Err(SourceError::BadRepoUrl(url.clone())),
                };
                let repo = match segments.next() {
                    Some(segment) => segment,
                    None => return Err(SourceError::BadRepoUrl(url.clone())),
                };

                Ok(RepoSourceNamed {
                    owner: owner.to_owned(),
                    repo: repo.to_owned(),
                })
            }
        }
    }
}

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum SourceError {
    #[error(transparent)]
    UrlParse(#[from] url::ParseError),
    #[error("Bad repo format: {0}, expected: <owner>/<repo>")]
    BadRepoFormat(String),
    #[error("Bad repo URL: {0}, expected: https://github.com/<owner>/<repo>")]
    BadRepoUrl(url::Url),
}
