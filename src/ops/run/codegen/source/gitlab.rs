use std::str::FromStr;

use serde::Deserialize;

use super::SourcePath;
use super::{Error, Result};

#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
pub struct Source {
    #[serde_as(as = "serde_with::FromInto<RepoSource>")]
    repo: RepoSourceNamed,
    #[serde(alias = "ref")]
    reference: Option<String>,
    #[serde(default = "Source::default_clone")]
    clone: bool,
}

impl Source {
    #[inline]
    fn default_clone() -> bool {
        true
    }
}

impl Source {
    pub async fn run(&self) -> Result<SourcePath> {
        let repo_url = self.repo.to_url();

        if self.clone {
            use super::git::Source as GitSource;

            let source = GitSource {
                url: repo_url,
                reference: self.reference.clone(),
            };

            source.run().await
        } else {
            use dircpy::CopyBuilder;

            use super::archive::Source as ArchiveSource;

            let default_branch = self.fetch_default_branch().await?;
            // https://gitlab.com/vigigloo/cli/-/archive/new/cli-new.tar.gz

            let reference = self
                .reference
                .as_deref()
                .or(default_branch.as_deref())
                .unwrap_or("main");
            let url = format!(
                "{repo_url}/-/archive/{reference}/{repo_name}-{reference}.tar.gz",
                repo_name = self.repo.name()
            )
            .parse()
            .expect("URL should already be valid");

            let source = ArchiveSource { url };
            let temp_source = source.run().await?;
            let temp_source = temp_source.join(format!(
                "{repo_name}-{reference}",
                repo_name = self.repo.name()
            ));

            let source = SourcePath::new()?;

            CopyBuilder::new(&temp_source, &source)
                .overwrite(true)
                .run()
                .map_err(|err| Error::copy_directory(err, temp_source, source.as_path()))?;

            Ok(source)
        }
    }

    async fn fetch_default_branch(&self) -> Result<Option<String>> {
        use gitlab::{
            api::{projects, AsyncQuery},
            GitlabBuilder, Project,
        };

        let client = GitlabBuilder::new_unauthenticated(self.repo.host())
            .build_async()
            .await
            .map_err(|err| Error::gitlab(err, "Could not build GitLab client"))?;

        let project: Project = projects::Project::builder()
            .build()
            .map_err(|err| Error::gitlab_boxed(err, "Could not construct project endpoint"))?
            .query_async(&client)
            .await
            .map_err(|err| {
                Error::gitlab(
                    err,
                    format!("Could not fetch project: {}", self.repo.to_url()),
                )
            })?;

        Ok(project.default_branch)
    }
}

impl FromStr for Source {
    type Err = url::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use std::collections::BTreeMap;

        let repo: RepoSourceNamed = RepoSource::from_str(s)?.into();

        let url = repo.to_url();
        let query_map: BTreeMap<_, _> = url.query_pairs().collect();

        let reference = query_map.get("ref");

        Ok(Source {
            repo,
            reference: reference.map(|reference| reference.to_string()),
            clone: Self::default_clone(),
        })
    }
}

#[derive(Debug, serde_with::DeserializeFromStr)]
enum RepoSource {
    Name(String),
    Url(url::Url),
}

impl FromStr for RepoSource {
    type Err = url::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("https") {
            Ok(RepoSource::Url(s.parse()?))
        } else {
            Ok(RepoSource::Name(s.to_owned()))
        }
    }
}

#[derive(Debug)]
pub struct RepoSourceNamed {
    host: Option<String>,
    pub path: Vec<String>,
}

impl RepoSourceNamed {
    fn to_url(&self) -> url::Url {
        let host = self.host.as_deref().unwrap_or("gitlab.com");
        let path = self.path.join("/");

        format!("https://{host}/{path}").parse().unwrap()
    }

    fn name(&self) -> String {
        self.path.last().expect("No repository").clone()
    }

    fn host(&self) -> &str {
        self.host.as_deref().unwrap_or("gitlab.com")
    }
}

impl From<RepoSource> for RepoSourceNamed {
    fn from(source: RepoSource) -> Self {
        match source {
            RepoSource::Name(name) => Self {
                host: None,
                path: name.split('/').map(ToOwned::to_owned).collect(),
            },
            RepoSource::Url(url) => Self {
                host: url.host_str().map(ToOwned::to_owned),
                path: url
                    .path_segments()
                    .map(|path| path.map(ToOwned::to_owned).collect())
                    .unwrap_or_default(),
            },
        }
    }
}
