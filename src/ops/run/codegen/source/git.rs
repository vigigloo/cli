use std::str::FromStr;

use serde::Deserialize;

use super::SourcePath;
use super::{Error, Result};

#[derive(Debug, Deserialize)]
pub struct Source {
    pub url: url::Url,
    #[serde(alias = "ref")]
    pub reference: Option<String>,
}

impl FromStr for Source {
    type Err = url::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Source {
            url: s.parse()?,
            reference: None,
        })
    }
}

impl Source {
    pub async fn run(&self) -> Result<SourcePath> {
        use git2::build::RepoBuilder;

        let path = SourcePath::new()?;

        tracing::info!("Fetching repository `{}` to {}", self.url, path.display());

        let mut builder = RepoBuilder::new();

        let repo = builder
            .clone(self.url.as_ref(), &path)
            .map_err(|err| Error::git(err, format!("Could not clone repository: {}", self.url)))?;

        if let Some(ref refname) = self.reference {
            let reference = repo
                .resolve_reference_from_short_name(refname)
                .map_err(|err| Error::git(err, format!("Could not find reference `{refname}`")))?;
            let object = reference.peel(git2::ObjectType::Any).map_err(|err| {
                Error::git(err, format!("Could not resolve reference `{refname}`"))
            })?;
            repo.checkout_tree(&object, None).map_err(|err| {
                Error::git(
                    err,
                    format!("Could not checkout repository to ref `{refname}`"),
                )
            })?;
        }

        tokio::fs::remove_dir_all(path.join(".git"))
            .await
            .map_err(|err| Error::io(err, "Could not delete .git directory"))?;

        Ok(path)
    }
}
