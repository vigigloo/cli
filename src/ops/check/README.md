# Check command

Displays last updates of sources from config file

## Usage

<i>Example : check for updates from last 24 hours</i>

```shell
$ vigigloo check
```

<i>Example : check for updates since January 5</i>

```shell
$ vigigloo check --since 2023-01-05
```

<i>Example : check for updates from last 5 months and exclude all "nightly" versions</i>

```shell
$ vigigloo check -l 5months -e "nightly"
```

## Config file

Config file must contain an entry `check` with list of sources.\
A source must be described with a `source` entry (the URL) and optionally :

- a `label`
- an `include` : regex that every version must match
- an `exclude` : regex that every version must not match\
  If no option is given, URL can be given directly (without `source` entry).

<i>Example</i>

```yaml
check:
  sources:
    - git+https://github.com/example-repository
    - label: example
      source: git+https://github.com/example-repository
      include: ^14\.[0-9]*\.[0-9]*.*$
      exclude: beta
```

### Sources

Sources can be git, helm, npm or docker repositories.

- npm sources structure : `npm://<PACKAGE NAME>` or `npm+<REGISTRY URL>/<PACKAGE NAME>`
- helm sources structure : `helm+<REGISTRY URL>#name=<CHART NAME>`
- git sources structure : `git+<REGISTRY URL>/<PACKAGE NAME>`

<i>Example</i>

```yaml
check:
  sources:
    - git+https://github.com/example-repository
    - npm+https://registry.npmjs.org/example-repository
    - npm://example-repository
    - helm+https://charts.example-repository.org#name=example-chart
```

### Outputs

Results can be rendered in a mattermost channel, an element room or sent by mail.\
In config file, `check` must contain a matching `outputs` entry.

<i>Example</i>

```yaml
check:
  sources:
    - git+https://github.com/example-repository
  outputs:
    mattermost: !Mattermost
      instance_url: https://most.bismuth.it
      api_key: "api_key"
      channel_id: "channel_id"
      message_template: |
        There was {{ count }} new packages versions {%if since %}since {{ date }}{% else %}in the last {{ duration }}{% endif %}
        {{ versions_table }}
    element: !Element
      user_id: "user_id"
      password: "user_password"
      room_id: "room_id"
      send_empty: false
      message_template: |
        There was {{ count }} new packages versions in the last {{ duration }} since {{ date }}
        {{ versions_table }}
    mail: !Mail
      smtp_server: "smtp.gmail.com"
      sender: "name@gmail.com"
      sender_password: "password"
      recipients:
        - "recipient1@domain.com"
        - "recipient2@domain.com"
      subject: "subject"
      send_empty: true
      tls: true
      message_template: |
        There was {{ count }} new packages versions in the last {{ duration }} since {{ date }}
        {{ versions_table }}
```

Check command must be called with `--output` option and a corresponding output in config file.\
<i>Example</i>

```shell
$ vigigloo check -o mattermost
```

`send_empty` entry (optional, default is `false`) is available for each output. If true, a message will be sent even if there is no update.

Template available variables for entry `message_template`:

- count: number of new packages versions found.
- since: true if the `since` CLI option has been filled in
- last: true if the `last` CLI option has been filled in
- date: the `since` CLI option if it has been filled in.
  The `last`CLI option converted as a date if it has been filled in. If no option was filled in, the date of yesterday.
- duration: The `last`CLI option if it has been filled in.
  The `since` CLI option converted as a duration if it has been filled in.
  If no option was filled in, the duration from yesterday
- versions_table: the table of versions found, with labels and sources.

All dates are in the format `dd/MM/yyyy at HH:mm:ss`.\
All durations are in the format `x years x months x days xxh xxm xxs`.

###### Mails

- `subject` entry is optional.
- `sender` and each recipient from `recipients`entry could either be :
  - a mail address, example : `name@domain.com`
  - a name and a mail address, example: `nickname <name@domain.com>`
- `tls` (`true` as default) : will use TLS as security protocol, or StartTLS if `false`.
