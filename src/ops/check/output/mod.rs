use chrono::{DateTime, Local};
use comfy_table::Table;
use serde::Deserialize;
use std::time::Duration;
use table_to_html::HtmlTable;

use super::VersionedSource;
use crate::ops::check::{Options, Result};

pub mod display;
pub mod element;
pub mod mail;
pub mod mattermost;

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub enum OutputKind {
    Mattermost(mattermost::OutputConfig),
    Element(element::OutputConfig),
    Mail(mail::OutputConfig),
}

pub(crate) enum TableFormat {
    Html,
    ComfyTable,
}

pub(crate) fn render_message(
    opts: &Options,
    sources: &[VersionedSource],
    template: &str,
    table_format: TableFormat,
) -> Result<String> {
    use tera::{Context, Tera};

    let count: usize = sources.iter().map(|e| e.versions.len()).sum();

    let (duration, date) = compute_duration_and_date(opts.last, opts.since)?;

    let table = if !sources.is_empty() {
        match table_format {
            TableFormat::Html => setup_table_html(sources).to_string(),
            TableFormat::ComfyTable => {
                let mut table = Table::new();
                table
                    .load_preset(comfy_table::presets::ASCII_MARKDOWN)
                    .set_content_arrangement(comfy_table::ContentArrangement::DynamicFullWidth)
                    .set_width(120);

                setup_table(table, sources).to_string()
            }
        }
    } else {
        "".to_string()
    };

    let mut tera = Tera::default();
    tera.add_raw_template("mattermost_template", template)?;

    let mut context = Context::new();
    context.insert("count", &count);
    context.insert("since", &opts.since);
    context.insert("last", &opts.last);
    context.insert("duration", &duration);
    context.insert("date", &date);
    context.insert("versions_table", &table);

    Ok(tera.render("mattermost_template", &context)?)
}

pub fn create_table_with_defaults(sources: &[VersionedSource]) -> Table {
    use comfy_table::{presets::UTF8_FULL, ContentArrangement};

    let mut table = Table::new();
    table
        .load_preset(UTF8_FULL)
        .set_content_arrangement(ContentArrangement::DynamicFullWidth)
        .set_width(100);

    setup_table(table, sources)
}

pub fn setup_table(mut table: Table, sources: &[VersionedSource]) -> Table {
    table.set_header(vec!["Label", "Version", "Source"]);

    for source in sources.iter() {
        for version in source.versions.iter() {
            table.add_row([
                source.source.label_or_source(),
                version.name(),
                source.source.source.as_str(),
            ]);
        }
    }

    table
}

pub fn setup_table_html(sources: &[VersionedSource]) -> HtmlTable {
    let mut table = vec![["Label", "Version", "Source"]];

    for source in sources.iter() {
        for version in source.versions.iter() {
            table.push([
                source.source.label_or_source(),
                version.name(),
                source.source.source.as_str(),
            ]);
        }
    }

    HtmlTable::new(table)
}

pub(crate) fn compute_duration_and_date(
    last: Option<Duration>,
    since: Option<DateTime<Local>>,
) -> Result<(String, String)> {
    use crate::util::date::{
        date_from_duration, date_to_formatted_string, duration_from_date,
        duration_to_formatted_string,
    };
    use chrono::Duration as chronoDuration;

    Ok(match (last, since) {
        (None, None) => (
            duration_to_formatted_string(Duration::new(86400, 0)),
            date_to_formatted_string(Local::now() - chronoDuration::days(1)),
        ),
        (None, Some(since)) => (
            duration_to_formatted_string(duration_from_date(since)?),
            date_to_formatted_string(since),
        ),
        (Some(last), _) => (
            duration_to_formatted_string(last),
            date_to_formatted_string(date_from_duration(last)?),
        ),
    })
}
