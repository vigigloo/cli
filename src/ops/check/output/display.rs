use crate::ops::check::output::create_table_with_defaults;
use crate::ops::check::VersionedSource;

pub fn display(sources: &[VersionedSource]) {
    if !sources.is_empty() {
        let table = create_table_with_defaults(sources);
        println!("{table}");
    } else {
        println!("No update");
    }
}
