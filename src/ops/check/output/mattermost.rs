use serde::Deserialize;

use crate::ops::check::output::{render_message, TableFormat};
use crate::ops::check::Result;
use crate::ops::check::{Options, VersionedSource};

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct OutputConfig {
    pub api_key: String,
    pub instance_url: url::Url,
    pub channel_id: String,
    pub message_template: Option<String>,
    /// default false
    pub send_empty: Option<bool>,
}

const DEFAULT_MESSAGE_TEMPLATE: &str = "\
There was {{ count }} new packages versions {%if since %}since {{ date }}{% else %}in the last {{ duration }}{% endif %}

{{ versions_table }}
";

pub async fn output_mattermost(
    opts: &Options,
    sources: &[VersionedSource],
    config: &OutputConfig,
) -> Result<()> {
    if !config.send_empty.unwrap_or(false) && sources.is_empty() {
        println!("No updates, no mail has been sent.");
        return Ok(());
    }

    let msg = render_message(
        opts,
        sources,
        config
            .message_template
            .as_deref()
            .unwrap_or(DEFAULT_MESSAGE_TEMPLATE),
        TableFormat::ComfyTable,
    )?;

    send_message(opts, config, msg).await?;

    Ok(())
}

async fn send_message(_opts: &Options, config: &OutputConfig, msg: impl AsRef<str>) -> Result<()> {
    use mattermost_client::Client;

    let client = Client::new(config.instance_url.as_str()).with_access_token(&config.api_key);
    client
        .posts()
        .create_post()
        .channel_id(&config.channel_id)
        .message(msg.as_ref())
        .build()
        .send(&client)
        .await?;

    Ok(())
}
