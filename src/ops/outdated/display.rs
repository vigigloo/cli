use crate::sources::{print_errors, resolve::ResolvedSource, VersionedSource};
use crate::{error::ErrorWithData, Results};

pub fn display_results(results: Results<ResolvedSource, ErrorWithData<VersionedSource>>) {
    use itertools::Itertools;

    let (sources, errors): (Vec<_>, Vec<_>) = results.split();

    let sources_groups = sources
        .into_iter()
        .into_group_map_by(|entry| entry.source.source.to_string())
        .into_iter()
        .sorted_unstable_by_key(|(key, _)| key.clone());

    for (kind, sources) in sources_groups {
        print_table(kind, sources);
    }

    print_errors(errors);
}

fn print_table(kind: String, sources: Vec<ResolvedSource>) {
    use comfy_table::{presets::UTF8_FULL, ColumnConstraint::*, ContentArrangement, Table};

    let mut table = Table::new();
    let is_inline_hint = kind == *"Inline Hints".to_string();

    let header = if is_inline_hint {
        vec![
            kind.as_str(),
            "Current",
            "Constraint",
            "Wanted",
            "Latest",
            "Location",
        ]
    } else {
        vec![kind.as_str(), "Current", "Wanted", "Latest", "Location"]
    };

    table
        .load_preset(UTF8_FULL)
        .set_content_arrangement(ContentArrangement::DynamicFullWidth)
        .set_width(100)
        .set_header(header)
        .column_mut(0)
        .expect("this should be first column")
        .set_constraint(UpperBoundary(comfy_table::Width::Fixed(30)));

    sources.iter().for_each(|s| {
        table.add_row(s.to_string_vec(is_inline_hint));
    });

    println!("{table}");
}

#[easy_ext::ext]
impl ResolvedSource {
    fn to_string_vec(&self, with_constraint: bool) -> Vec<String> {
        let current = self.source.current.as_deref().unwrap_or("N/A");
        let wanted = self
            .wanted
            .as_ref()
            .map(|version| version.to_string())
            .unwrap_or_else(|| "N/A".to_string());

        if with_constraint {
            let constraint = self.wanted.as_deref().unwrap_or("N/A");
            vec![
                self.source.source.describe(),
                current.to_string(),
                constraint.to_string(),
                wanted,
                self.latest.to_string(),
                self.source.source.to_string(),
            ]
        } else {
            vec![
                self.source.source.describe(),
                current.to_string(),
                wanted,
                self.latest.to_string(),
                self.source.source.loc.as_ref().unwrap().to_string(),
            ]
        }
    }
}
