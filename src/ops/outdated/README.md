# Outdated command

Displays outdated modules/providers from terraform files and outdated dependencies from helm charts.

## Usage
```shell
$ vigigloo outdated
```
