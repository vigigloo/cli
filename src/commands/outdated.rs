use clap::Parser;
use miette::Result;

#[derive(Debug, Parser)]
pub struct Command {
    /// Parse Terraform files and helm charts recursively
    #[arg(short, long)]
    recursive: bool,
    /// Parse Terraform files and helm charts inside hidden directories
    #[arg(short = 'H', long)]
    hidden: bool,
}

impl Command {
    pub async fn run(self) -> Result<()> {
        use vigigloo::{ops::outdated, sources::parse::ParseOptions};

        outdated::run(outdated::Options {
            parse: ParseOptions {
                recursive: self.recursive,
                hidden: self.hidden,
            },
            ..Default::default()
        })
        .await?;

        Ok(())
    }
}
