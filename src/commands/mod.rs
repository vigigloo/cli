use clap::Subcommand;
use miette::Result;

use vigigloo::Config;

mod debug;

mod assist;
mod check;
mod error;
mod outdated;
mod run;
mod update;

#[derive(Debug, Subcommand)]
pub enum Command {
    Debug(debug::Command),

    /// Tasks executor using the workflow entry in config file as a playbook.
    Run(run::Command),

    /// * Deprecated * Tasks executor using the workflow entry in config file as a playbook. This command is deprecated and will be removed in v2.0"
    Assist(assist::Command),

    /// Displays outdated modules/providers from terraform files and outdated dependencies from helm charts.
    Outdated(outdated::Command),

    /// Updates outdated modules/providers from terraform files and outdated dependencies from helm charts. Versions that use version matching aren't updated.
    Update(update::Command),

    /// Displays last updates of sources from config file.
    Check(check::Command),
}

impl Command {
    pub async fn run(self, config: Option<&Config>) -> Result<()> {
        match self {
            Command::Debug(subcommand) => subcommand.run().await,
            Command::Assist(subcommand) => subcommand.run(config).await,
            Command::Run(subcommand) => subcommand.run(config).await,
            Command::Outdated(subcommand) => subcommand.run().await,
            Command::Update(subcommand) => subcommand.run().await,
            Command::Check(subcommand) => subcommand.run(config).await,
        }
    }
}
