#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum Error {
    #[error("No config file provided")]
    NoConfigFile,
}
