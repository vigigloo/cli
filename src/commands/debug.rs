#![allow(dead_code)]

use clap::Parser;
use miette::Result;

#[derive(Debug, Parser)]
pub struct Command {}

impl Command {
    pub async fn run(self) -> Result<()> {
        Ok(())
    }
}

#[cfg(disabled)]
#[serde_with::serde_as]
#[derive(Debug, Clone, serde::Deserialize)]
pub struct Source {
    pub label: Option<String>,
    pub source: url::Url,
    #[serde_as(as = "Option<serde_with::DisplayFromStr>")]
    pub include: Option<regex::Regex>,
    #[serde_as(as = "Option<serde_with::DisplayFromStr>")]
    pub exclude: Option<regex::Regex>,
}

#[cfg(feature = "debug-matrix")]
async fn debug_matrix() -> Result<()> {
    use matrix_sdk::{
        config::SyncSettings,
        room::Room,
        ruma::{self, events::room::message::RoomMessageEventContent},
        Client,
    };
    use miette::IntoDiagnostic;

    let user = ruma::user_id!("@tentative1:matrix.org");

    let client = Client::builder()
        .server_name(user.server_name())
        .build()
        .await
        .into_diagnostic()?;

    client
        .login_username(&user, "tentative0sur0")
        .send()
        .await
        .into_diagnostic()?;

    let room = ruma::room_alias_id!("#test-vigigloo:qonfucius.team");
    let room = client.resolve_room_alias(room).await.into_diagnostic()?;
    let room_id: &ruma::RoomId = room.room_id.as_ref();

    let room = client
        .join_room_by_id_or_alias(room_id.into(), &room.servers)
        .await
        .into_diagnostic()?;

    // !!! CETTE LIGNE EST TRES IMPORTANTE !!!
    client
        .sync_once(SyncSettings::default())
        .await
        .into_diagnostic()?;

    let Room::Joined(room) = client.get_room(&room.room_id).expect("Unknown room") else {
        miette::bail!("Room not joined!");
    };

    let content = RoomMessageEventContent::text_plain("Hello world!");
    room.send(content, None).await.into_diagnostic()?;

    client.logout().await.into_diagnostic()?;

    Ok(())
}
