use chrono::{DateTime, Local};
use clap::{builder::TypedValueParser, Parser};
use miette::Result;
use regex::Regex;

use vigigloo::{ops::check, Config};

pub fn parse_datetime(value: String) -> Result<DateTime<Local>, String> {
    use chrono::NaiveDate;

    if let Ok(datetime) = value.parse::<DateTime<Local>>() {
        Ok(datetime)
    } else {
        let date = value
            .parse::<NaiveDate>()
            .map_err(|err| format!("valid RFC3339-formatted date or datetime: {err}"))?;
        Ok(date
            .and_hms_opt(0, 0, 0)
            .unwrap()
            .and_local_timezone(Local)
            .unwrap())
    }
}

#[derive(Debug, Parser)]
pub struct Command {
    /// Defines the duration of the search window. Default 24h
    #[arg(
        short,
        long,
        env = "VIGIGLOO_CHECK_LAST",
        conflicts_with = "since",
        help_heading = "Filtering options"
    )]
    last: Option<humantime::Duration>,

    /// Defines the search window to since a specified date (formatted using RFC3339)
    #[arg(
        short = 'S',
        long,
        env = "VIGIGLOO_CHECK_SINCE",
        conflicts_with = "last",
        value_parser = clap::builder::StringValueParser::new().try_map(parse_datetime),
        help_heading = "Filtering options"
    )]
    since: Option<DateTime<Local>>,

    /// Output to alternative backends (this flag can be repeated to output to multiple backends)
    #[arg(
        short,
        long = "output",
        env = "VIGIGLOO_CHECK_OUTPUTS",
        value_name = "OUTPUT_NAME",
        value_delimiter = ','
    )]
    outputs: Vec<String>,

    /// Defines a filter to keep only versions that match the regex
    #[arg(
        short,
        long,
        env = "VIGIGLOO_CHECK_INCLUDE",
        value_name = "INCLUDE_REGEX"
    )]
    include: Option<Regex>,

    /// Defines a filter to keep only versions that don't match the regex
    #[arg(
        short,
        long,
        env = "VIGIGLOO_CHECK_EXCLUDE",
        value_name = "EXCLUDE_REGEX"
    )]
    exclude: Option<Regex>,

    /// External sources from git. If filled, it will be checked instead of config file
    sources: Vec<check::Source>,
}

impl Command {
    pub async fn run(self, config: Option<&Config>) -> Result<()> {
        let sources = match self.sources.as_slice() {
            [] => config
                .map(|config| config.check.sources.as_slice())
                .unwrap_or(&[]),
            sources => {
                if config.is_some() {
                    tracing::warn!("Configuration file has not been read");
                };

                sources
            }
        };

        let outputs = config
            .map(|config| config.check.outputs.clone())
            .unwrap_or_default();

        vigigloo::ops::check::run(
            check::Options {
                last: self.last.map(Into::into),
                since: self.since,
                outputs: self.outputs,
                include: self.include,
                exclude: self.exclude,
            },
            sources,
            outputs,
        )
        .await?;

        Ok(())
    }
}
