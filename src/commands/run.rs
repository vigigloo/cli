use super::error::Error;
use clap::Parser;
use miette::Result;

use vigigloo::Config;

#[derive(Debug, Parser)]
pub struct Command;

impl Command {
    pub async fn run(self, config: Option<&Config>) -> Result<()> {
        let config = config.ok_or(Error::NoConfigFile)?;

        tracing::debug!("Config: {config:#?}");

        Ok(vigigloo::ops::run::run(config).await?)
    }
}
