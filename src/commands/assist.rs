use clap::Parser;
use miette::Result;

use vigigloo::Config;

#[derive(Debug, Parser)]
pub struct Command {
    #[command(subcommand)]
    command: Option<Subcommand>,
}

#[derive(Debug, Clone, Copy, Default, Parser)]
enum Subcommand {
    #[default]
    All,
    Codegen,
}

impl Command {
    pub async fn run(self, config: Option<&Config>) -> Result<()> {
        let subcommand = self.command.unwrap_or_default();
        let config = config.expect("No config file provided");

        match subcommand {
            Subcommand::All => {
                vigigloo::ops::run::run(config).await?;

                Ok(())
            }
            Subcommand::Codegen => Ok(vigigloo::ops::run::run(config).await?),
        }
    }
}
