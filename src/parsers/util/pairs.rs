use pest::{
    iterators::{Pair, Pairs},
    RuleType,
};

pub fn next_opt<'i, R: RuleType>(pairs: &mut Pairs<'i, R>, rule: R) -> Option<Pair<'i, R>> {
    match pairs.peek() {
        Some(pair) if pair.as_rule() == rule => pairs.next(),
        _ => None,
    }
}
