use std::{cmp, ops};

use pest::{iterators::Pair, RuleType, Span};

#[derive(Debug, Clone, Copy)]
pub struct Spanned<'i, T>(Span<'i>, T);

impl<'i, T> Spanned<'i, T> {
    pub fn new(span: Span<'i>, value: impl Into<T>) -> Spanned<'i, T> {
        Spanned(span, value.into())
    }

    pub fn span(&self) -> Span<'i> {
        self.0
    }

    pub fn value(&self) -> &T {
        &self.1
    }

    pub fn into_value(self) -> T {
        self.1
    }

    pub fn into_pair(self) -> (Span<'i>, T) {
        (self.0, self.1)
    }

    pub fn as_ref(&self) -> Spanned<'i, &T> {
        Spanned(self.0, &self.1)
    }
}

impl<'i, R: RuleType> From<Pair<'i, R>> for Spanned<'i, &'i str> {
    fn from(pair: Pair<'i, R>) -> Self {
        Spanned(pair.as_span(), pair.as_str())
    }
}

impl<'i, T: ops::Deref> Spanned<'i, T> {
    pub fn as_deref(&self) -> Spanned<'i, &T::Target> {
        Spanned(self.0, self.1.deref())
    }
}

impl<'i, T> ops::Deref for Spanned<'i, T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &Self::Target {
        self.value()
    }
}

impl<'i, T: cmp::PartialEq<T>> cmp::PartialEq<T> for Spanned<'i, T> {
    #[inline]
    fn eq(&self, other: &T) -> bool {
        self.value().eq(other)
    }
}
