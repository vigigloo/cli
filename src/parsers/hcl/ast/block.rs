use super::error::Error;
use crate::parsers::{
    hcl::{
        ast::{Body, Str},
        Pair, Rule,
    },
    Spanned,
};
use std::ops;

#[derive(Debug, Clone)]
pub struct Block<'i> {
    pub id: Str<'i>,
    pub args: Vec<Str<'i>>,
    pub body: Body<'i>,
}

impl<'i> Block<'i> {
    pub(super) fn process(pair: Pair<'i>) -> (Self, Vec<Error>) {
        let mut items = pair.into_inner();

        let id = items.next().unwrap();

        let mut items: Vec<_> = items.collect();
        let body = items.pop().unwrap().into_inner().next().unwrap();

        let args = items;

        let (body, errors) = Body::process(body);

        let block = Self {
            id: Spanned::new(id.as_span(), id.as_str()),
            args: args
                .into_iter()
                .map(|arg| {
                    let span = arg.as_span();

                    let value = match arg.as_rule() {
                        Rule::StringLit => arg.into_inner().next().unwrap().as_str(),
                        Rule::Identifier => arg.as_str(),
                        rule => unreachable!("{rule:?}"),
                    };

                    Spanned::new(span, value)
                })
                .collect(),
            body,
        };

        (block, errors)
    }
}

impl<'i> ops::Deref for Block<'i> {
    type Target = Body<'i>;

    fn deref(&self) -> &Self::Target {
        &self.body
    }
}
