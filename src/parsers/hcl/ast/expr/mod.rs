use crate::parsers::{
    hcl::{ast::Str, Pair, Rule},
    util::pairs,
};

use self::term::ExprTerm;

pub mod term;

#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum Expression<'i> {
    Term(ExprTerm<'i>),
}

#[allow(irrefutable_let_patterns)]
impl<'i> Expression<'i> {
    pub fn as_term(&self) -> Option<&ExprTerm<'i>> {
        if let Self::Term(ref term) = self {
            Some(term)
        } else {
            None
        }
    }

    pub fn as_string(&self) -> Option<Str<'i>> {
        self.as_term().and_then(|term| term.as_string())
    }

    pub fn as_object(&self) -> Option<&term::object::Object<'i>> {
        self.as_term().and_then(|term| term.as_object())
    }

    pub fn as_tuple(&self) -> Option<&[Expression<'i>]> {
        self.as_term().and_then(|term| term.as_tuple())
    }
}

impl<'i> Expression<'i> {
    pub(super) fn process(pair: Pair<'i>) -> Self {
        let mut items = pair.into_inner();

        let _unary_op = pairs::next_opt(&mut items, Rule::UnaryOperator);
        let expr = items.next().unwrap();

        let expr = ExprTerm::process(expr);

        Expression::Term(expr)
    }
}
