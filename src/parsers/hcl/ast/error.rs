pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum Error {
    #[error("Error while parsing inline hint : {hint}\nline : {line}\nPattern not known.")]
    InlineHintPattern { hint: String, line: String },

    #[error("Multiple inline hint in a row :\n{hint}line : {line}.\nAn inline hint must be directly followed by a version entry.")]
    MultipleInlineHint { hint: String, line: String },
}
