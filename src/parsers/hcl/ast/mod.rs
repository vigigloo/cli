use super::ast::error::Error;
use super::Pair;
use crate::parsers::Spanned;

pub use self::attr::Attribute;
pub use self::block::Block;
pub use self::body::Body;
pub use self::hint::InlineHint;

mod attr;
mod block;
mod body;
pub mod error;
pub mod expr;
mod hint;

pub type Hcl<'i> = Body<'i>;

pub type Str<'i> = Spanned<'i, &'i str>;

#[inline]
pub(super) fn process(pair: Pair) -> (Hcl, Vec<Error>) {
    Hcl::process(pair)
}
