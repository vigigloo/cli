use super::error::{Error, Result};
use crate::parsers::hcl::{ast::Str, Pair, Rule};

#[derive(Debug, Clone)]
pub enum InlineHint<'i> {
    Source(SourceHint<'i>),
}

impl<'i> InlineHint<'i> {
    pub(super) fn process(pair: Pair<'i>) -> Result<Self> {
        let mut items = pair.into_inner();

        let kind = items.next().unwrap();

        match kind.as_rule() {
            Rule::SourceInlineHint => Ok(Self::Source(SourceHint::process(kind))),
            Rule::InlineHintError => {
                let (l, _) = kind.line_col();
                Err(Error::InlineHintPattern {
                    hint: kind.as_str().to_string(),
                    line: l.to_string(),
                })
            }
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct SourceHint<'i> {
    pub address: Str<'i>,
    pub version_constraint: Option<Str<'i>>,
}

impl<'i> SourceHint<'i> {
    pub(super) fn process(pair: Pair<'i>) -> Self {
        let mut source_inline_hint = pair.into_inner();
        let address = source_inline_hint.next().unwrap();
        let version_constraint = source_inline_hint.next();

        SourceHint {
            address: Str::from(address),
            version_constraint: version_constraint.map(Str::from),
        }
    }
}
