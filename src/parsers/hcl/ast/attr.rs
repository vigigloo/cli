use super::error::Result;
use super::InlineHint;
use crate::parsers::hcl::Rule;
use crate::parsers::{
    hcl::{
        ast::{expr::Expression, Str},
        Pair,
    },
    Spanned,
};

#[derive(Debug, Clone)]
pub struct Attribute<'i> {
    pub hint: Option<InlineHint<'i>>,
    pub name: Str<'i>,
    pub expr: Spanned<'i, Expression<'i>>,
}

impl<'i> Attribute<'i> {
    pub(super) fn process(pair: Pair<'i>) -> Result<Self> {
        let mut items = pair.into_inner();

        let mut hint = None;
        let mut item = items.next().unwrap();

        if item.as_rule() == Rule::InlineHint {
            hint = Some(InlineHint::process(item.clone())?);
            item = items.next().unwrap();
        }

        let name = item.clone();
        let expr = items.next().unwrap();

        Ok(Self {
            hint,
            name: Str::from(name),
            expr: Spanned::new(expr.as_span(), Expression::process(expr)),
        })
    }

    pub fn second_term_as_string(self) -> Option<Str<'i>> {
        self.expr.as_term().unwrap().as_string()
    }
}
