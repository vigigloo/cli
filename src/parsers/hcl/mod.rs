use super::hcl::ast::error::Error as AstError;
pub mod ast;

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
#[error(transparent)]
pub struct Error(#[from] Box<pest::error::Error<Rule>>);

impl From<pest::error::Error<Rule>> for Error {
    fn from(value: pest::error::Error<Rule>) -> Self {
        Box::new(value).into()
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(pest_derive::Parser)]
#[grammar = "parsers/hcl/hcl.pest"]
struct HclParser;

type Pair<'i> = pest::iterators::Pair<'i, Rule>;
type Pairs<'i> = pest::iterators::Pairs<'i, Rule>;

pub fn parse(input: &str) -> Result<(ast::Hcl, Vec<AstError>)> {
    use pest::Parser;

    let mut pairs = HclParser::parse(Rule::Hcl, input)?;
    Ok(ast::process(pairs.next().unwrap()))
}
